<?php

function posts()
{
    require_once "modeles/modele_posts.php";
    try {
        $posts = getPosts();
        $count = countComments();
        require 'view/post.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}

function mesPosts()
{
    require_once "modeles/modele_posts.php";
    try {
        $userPosts = getUserPosts();
        require "view/mesPosts.php";
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}

function showAPost()
{

    $id = $_POST['id'];
    require_once "modeles/modele_posts.php";
    try {
        $post = getAPost($id);
        require "view/aPost.php";
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}


function publicPost()
{

    $id = $_POST['id'];
    require_once "modeles/modele_posts.php";
    try {
        $comments = getComments();
        $post = getAPost($id);
        require "view/aPublicPost.php";
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}


function addAPost()
{
if($_FILES["image"]["tmp_name"]=''){
    $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));
}
else
    $file=null;
    $autor = $_SESSION["user"]["userEmailAddress"];
    $date = date_create()->format('Y-m-d');

    $today = getdate();
    $time=$today['hours'].':'.$today['minutes'].':'.$today['seconds'];
    $user_id= kownUserId();
    $post = $_POST['post'];
    require_once "modeles/modele_posts.php";
    try {
        insertAPost($autor,$post,$file,$user_id,$date,$time);
        header('view/post.php');
        posts();

    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}


function updateAPost()
{
    $id = $_POST['id'];
    $newPost = $_POST['post'];
    require_once "modeles/modele_posts.php";
    try {
        //$post =
        updatePost($id, $newPost);
        showAPost();
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php.php';
    }

}

function getComments()
{
    $idComment = (int)$_POST['id'];
    // $id = $_POST['id_comment'];
    //  $idComment = (int)$_POST['id_comment'];

    require_once "modeles/modele_posts.php";
    try {
        $allComments = getAllComments($idComment);

        return $allComments;

        // showAPost();
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function comment()
{
    $user_id= (int)kownUserId();
    // $id = $_POST['id_comment'];
    $idComment = (int)$_POST['id_comment'];
    $comment = $_POST['comment'];
    $autor = $_SESSION["user"]["userEmailAddress"];
    $date = date_create()->format('Y-m-d');
    $today = getdate();
    $time = $today['hours'] . ':' . $today['minutes'] . ':' . $today['seconds'];
    // $time = date_create()->format('Y-m-d');
    require_once "modeles/modele_posts.php";
    try {
        addAComment($comment, $idComment,$user_id, $autor, $date, $time);
        publicPost();
        //   require "view/aPublicPost.php";
        // showAPost();
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function kownUserId()
{


    $userEmailAddress = $_SESSION['user']['userEmailAddress'];

    require 'modeles/modele_users.php';

    try {
        $userId = getUserId($userEmailAddress);
        return $userId;
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }


}