<?php




if (isset($_GET['action'])) {
    switch ($_GET['action']) {



        case 'logout' :
            require 'controler/users_controler.php';

            logout();
            break;

        case 'login' :
            require 'controler/users_controler.php';
            login($_POST);
            break;

        case 'register' :
            require 'controler/users_controler.php';
            register($_POST);
            break;

        case 'details' :
            require 'controler/users_controler.php';
            details();
            break;

        default :
            require 'controler/users_controler.php';
            home();
            break;

    }
} else {
    require 'controler/users_controler.php';
    home();
}
