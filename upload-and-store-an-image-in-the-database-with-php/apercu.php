<?php
if ( isset($_GET['id']) ){
    $id = intval ($_GET['id']);
    include ("config.php");

    $req = "SELECT id,name,image" .
        "FROM images WHERE id = " . $id;
    $ret = mysqli_query ($con, $req) or die (mysqli_error ());
    $col = mysqli_fetch_row ($ret);

    if ( !$col[0] ){
        echo "Id d'image inconnu";
    } else {
        header ("Content-type: " . $col[1]);
        echo '<img src="' . $col[2] . '" />';
    }
} else {
    echo "Mauvais id d'image";
}
?>
