<?php
/**
 * Author: Pascal.benzonana@cpnv.ch
 * Project  : snowUser.php
 * Created  : 04.03.2019 - 22:13
 *
 * Last update :    [18.02.2019 pba]
 *                  [modele.php splitted]
 * Git source  :    [link]
 */

$titre="";
// Tampon de flux stocké en mémoire
ob_start();
$cols=0; // Column count
?>

<article>
    <header>
        <h2> Nos snows</h2>
        <div class="yox-view">

            <?php foreach ($snowsResults as $result) : ?>
                <?php $cols++; ?>
                <?php if ($cols%4) : // tests to have 4 items / line ?>
                <div class="row-fluid">
                    <ul class="thumbnails">
                    <?php $cols=0;?>
                <?php endif ?>

                        <li class="span3">
                            <div class="thumbnail">
                                <a href="view/content/images/<?= $result['code']; ?>.png" target="blank"><img src="<?= $result['photo']; ?>" alt="<?= $result['code']; ?>" ></a>
                                <div class="caption">
                                    <h3><a href="index.php?action=displayASnow&code=<?= $result['code']; ?>"><?= $result['code']; ?></a></h3>
                                    <p><strong>Marque : </strong><?= $result['brand']; ?></p>
                                    <p><strong>Modèle : </strong><?= $result['model']; ?></p>
                                    <p><strong>Longueur : </strong><?= $result['snowLength']; ?> cm</p>
                                    <p><strong>Prix :</strong> CHF <?= $result['dailyPrice']; ?>.- / jour</p>
                                    <p><strong>Disponibilité : </strong><?= $result['qtyAvailable']; ?></p>
                                </div>
                            </div>
                        </li>

                <?php if ($cols%4) :?>
                     </ul>
                </div>
                <?php endif ?>
            <?php endforeach ?>

        </div>
    </header>
</article>
<hr/>

<?php
$contenu = ob_get_clean();

?>

