<?php

function posts()
{
    require_once "modeles/modele_posts.php";
    try {
        $posts = getPosts();
        require 'view/post.php';
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function mesPosts()
{
    require_once "modeles/modele_posts.php";
    try {
        $userPosts = getUserPosts();
        require "view/mesPosts.php";
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function showAPost()
{
    $id=$_POST['id'];
    require_once "modeles/modele_posts.php";
    try {
        $post = getAPost($id);
        require "view/aPost.php";
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function updateAPost()
{
    $id=$_POST['id'];
    $newPost=$_POST['post'];
    require_once "modeles/modele_posts.php";
    try {
        $post = updatePost($id,$newPost);
        showAPost();
    } catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}