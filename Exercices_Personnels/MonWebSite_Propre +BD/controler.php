<?php
function home()
{
    require "home.php";
}

function details()
{
    require "details.php";
}

function meteo()
{
    require "meteo.php";
}

function transports()
{
    require "transports.php";
}

function nouveau()
{
    require "nouveau.php";
}
function listeDePresence()
{
    require "liste_de_presence.php";
}

function wantedLogin()
{
    login();
    require "functions.php";
}

function goToUploadImage()
{
    require "ImageStorer/index_image.php";
}

function adminPage()
{
    require "adminPage.php";
}

function listeImage()
{
    require "ImageStorer/liste.php";
}


function posts()
{
    require "post_index.php";
}

function mesPosts()
{
    require "mesPosts.php";
}

