MonWebSite_Propre+BD
---
Cette application à besoin des configuration suivantes pour fonctionner :
---
Dans le fichier "php.ini" Decommenter les extensions suivantes :

* Mysqli
* OpenSSL
___

Deployez un serveur Xampp avec les services suivants :
* Appache
* MySql
___

Dans le le fichier "my.ini" du service MySql :
* max_allowed_packet=128M
* innodb_log_file_size=128M
___

Demarrer une session serveur PhpMyAdmin en localhost avec la configuration suivante:
* login :     "root"
* psw :        ""
___

Creez 3 bases de donnee suivants avec les noms suivants : 
* crud
* multi_login
* images
___

Lancez les script suivants dans le dossier SQL :
* images.sql
* multi_login_tables_user+image.sql
* user_posts.sql
___




Pour inserrer et voir la liste des images (ici les images sonts stocké dans un fichier ) :
--

* etre connecte en Admin (pour le Mdp se creer un compte un User et changer le user_type manuellement dans la DB)
* Dans le menu pricipale (en haut a droite) des section ont apparuent :
    * se rendre  dans -> User Page
    * inserrer une image
    * vous pouvez choisir votre photo de profil en cliquant sur select en dessous de l image


___
Questions pour M.Glassey:
---

Dans quel cas stocker images en blob dans DB ou dans un fichier ?
avantages/inc

BD unique pour toutes les images (users et posts) ?

critiques sur :
    demande d'aide apprpriée ou non 
    formulation de la demande 
    infos données, qualité, présision, liens
    le fait de demander de l aide sur un projet non structuré
    qualité  du ReadMe
    toutes autres possibles