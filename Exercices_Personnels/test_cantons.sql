-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 23 nov. 2019 à 18:34
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test_cantons`
--

-- --------------------------------------------------------

--
-- Structure de la table `cantons`
--

CREATE TABLE `cantons` (
  `id` smallint(6) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(28) NOT NULL,
  `since` smallint(6) NOT NULL,
  `capital` varchar(11) DEFAULT NULL,
  `population` mediumint(9) DEFAULT NULL,
  `area` decimal(7,2) DEFAULT NULL,
  `density` decimal(5,2) DEFAULT NULL,
  `altMax` smallint(6) DEFAULT NULL,
  `altMin` smallint(6) DEFAULT NULL,
  `altMoy` smallint(6) DEFAULT NULL,
  `altDiff` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `cantons`
--

INSERT INTO `cantons` (`id`, `code`, `name`, `since`, `capital`, `population`, `area`, `density`, `altMax`, `altMin`, `altMoy`, `altDiff`) VALUES
(2, 'AI', 'Appenzell Rhodes-Intérieures', 1513, 'Appenzell', 16003, '172.52', '92.80', 2502, 560, 1126, 1942),
(3, 'AR', 'Appenzell Rhodes-Extérieures', 1513, 'Herisau', 54954, '242.86', '226.30', 2502, 440, 935, 2060),
(4, 'BE', 'Berne', 1353, 'Berne', 1026513, '5959.44', '172.20', 4274, 402, 1198, 3872),
(5, 'BL', 'Bâle-Campagne', 1501, 'Liestal', 286848, '517.56', '554.20', 1169, 246, 521, 923),
(6, 'BS', 'Bâle-Ville', 1501, 'Bâle', 198249, '37.00', '999.99', 522, 245, 295, 277),
(7, 'FR', 'Fribourg', 1481, 'Fribourg', 311914, '1670.70', '186.70', 2389, 429, 856, 1960),
(8, 'GE', 'Genève', 1815, 'Genève', 495325, '282.48', '999.99', 516, 332, 419, 184),
(9, 'GL', 'Glaris', 1352, 'Glaris', 40147, '685.30', '58.60', 3614, 410, 1589, 3204),
(10, 'GR', 'Grisons', 1803, 'Coire', 197550, '7105.44', '27.80', 4049, 260, 2021, 3789),
(11, 'JU', 'Jura', 1979, 'Delémont', 73122, '838.55', '87.20', 1302, 364, 690, 938),
(12, 'NE', 'Neuchâtel', 1815, 'Neuchâtel', 178567, '802.93', '222.40', 1552, 429, 919, 1123),
(13, 'NW', 'Nidwald', 1291, 'Stans', 42556, '275.90', '154.20', 2901, 434, 1077, 2467),
(14, 'OW', 'Obwald', 1291, 'Sarnen', 37378, '490.59', '76.20', 3238, 434, 1329, 2804),
(16, 'SG', 'Saint-Gall', 1803, 'Saint-Gall', 502552, '2025.54', '248.10', 3248, 396, 1000, 2852),
(17, 'SH', 'Schaffhouse', 1501, 'Schaffhouse', 80769, '298.42', '270.70', 912, 344, 538, 568),
(18, 'SO', 'Soleure', 1481, 'Soleure', 269441, '790.49', '340.90', 1445, 277, 630, 1168),
(19, 'SZ', 'Schwytz', 1291, 'Schwytz', 155863, '906.92', '171.90', 2802, 406, 1082, 2396),
(20, 'TG', 'Thurgovie', 1803, 'Frauenfeld', 270709, '991.02', '273.20', 991, 370, 495, 621),
(21, 'TI', 'Tessin', 1803, 'Bellinzone', 354375, '2812.20', '126.00', 3402, 193, 1412, 3209),
(22, 'UR', 'Uri', 1291, 'Altdorf', 36145, '1076.57', '33.60', 3630, 434, 1896, 3196),
(23, 'VD', 'Vaud', 1803, 'Lausanne', 784822, '3212.03', '244.30', 3210, 372, 827, 2838),
(24, 'VS', 'Valais', 1815, 'Sion', 339176, '5224.25', '64.90', 4634, 372, 2140, 4262),
(25, 'ZG', 'Zoug', 1352, 'Zoug', 123948, '238.69', '519.30', 1580, 388, 651, 1192),
(26, 'ZH', 'Zurich', 1351, 'Zurich', 1487969, '1729.00', '860.60', 1292, 332, 533, 960),
(28, 'LU', 'Lucerne', 1332, 'Lucerne', 403397, '1493.44', '270.11', 2350, 406, 777, 1944),
(5555, 'AG', 'Argovie', 1803, 'Aarau', 662224, '1403.73', '471.80', 908, 260, 476, 648);

-- --------------------------------------------------------

--
-- Structure de la table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `rankpop` int(11) DEFAULT NULL,
  `cantons_id` smallint(6) NOT NULL,
  `population` int(11) DEFAULT NULL,
  `area` float DEFAULT NULL,
  `density` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `cities`
--

INSERT INTO `cities` (`id`, `name`, `rankpop`, `cantons_id`, `population`, `area`, `density`) VALUES
(2, 'Adliswil', 55, 26, 18742, 7.77, 2412.1),
(3, 'Aesch', 143, 5, 10267, 7.39, 1389.3),
(4, 'Affoltern am Albis', 116, 26, 11900, 10.61, 1121.6),
(5, 'blablabla', 147, 23, 9961, 16.41, 607),
(6, 'Allschwil', 42, 5, 20849, 8.92, 2337.3),
(7, 'Altdorf', 156, 22, 9211, 10.21, 902.2),
(8, 'Altstätten', 127, 16, 11438, 39.49, 289.6),
(9, 'Amriswil', 97, 20, 13346, 19.02, 701.7),
(10, 'Appenzell', 167, 2, 5825, 16.86, 345.5),
(11, 'Arbon', 87, 20, 14340, 5.94, 2414.1),
(12, 'Arlesheim', 155, 5, 9274, 6.93, 1338.2),
(13, 'Arosa', 172, 10, 3219, 154.78, 20.8),
(14, 'Arth', 118, 19, 11735, 42.08, 278.9),
(15, 'Baar', 33, 25, 24129, 24.83, 971.8),
(16, 'Baden', 50, 5555, 19122, 13.18, 1450.8),
(17, 'Bâle', 3, 6, 175940, 23.91, 7358.4),
(18, 'Bassersdorf', 121, 26, 11616, 9.02, 1287.8),
(19, 'Bellinzone', 12, 21, 42615, 164.91, 258.4),
(20, 'Belp', 124, 4, 11534, 23.29, 495.2),
(21, 'Berne', 5, 4, 133115, 51.62, 2578.7),
(22, 'Berthoud', 72, 4, 16295, 15.6, 1044.6),
(23, 'Bienne', 10, 4, 54456, 21.23, 2565),
(24, 'Binningen', 82, 5, 15461, 4.43, 3490.1),
(25, 'Birsfelden', 138, 5, 10531, 2.52, 4179),
(26, 'Brigue-Glis', 100, 24, 13158, 38.08, 345.5),
(27, 'Brugg', 131, 5555, 11172, 6.36, 1756.6),
(28, 'Buchs', 107, 16, 12531, 15.95, 785.6),
(29, 'Bulle', 36, 7, 22523, 23.87, 943.6),
(30, 'Bussigny', 160, 23, 8268, 4.82, 1715.4),
(31, 'Bülach', 48, 26, 19611, 16.09, 1218.8),
(32, 'Carouge', 38, 8, 22251, 2.7, 8241.1),
(33, 'Cham', 73, 25, 16216, 17.71, 915.6),
(34, 'Chêne-Bougeries', 119, 8, 11669, 4.13, 2825.4),
(35, 'Chiasso', 159, 21, 8331, 5.33, 1563),
(36, 'Coire', 18, 10, 34880, 28.09, 1241.7),
(37, 'Crissier', 162, 23, 7869, 5.51, 1428.1),
(38, 'Davos', 133, 10, 11060, 283.98, 38.9),
(39, 'Delémont', 106, 11, 12562, 21.99, 571.3),
(40, 'Dietikon', 28, 26, 27076, 9.33, 2902),
(41, 'Dübendorf', 26, 26, 27689, 13.62, 2033),
(42, 'Ebikon', 99, 28, 13313, 9.68, 1375.3),
(43, 'Écublens', 109, 23, 12342, 5.71, 2161.5),
(44, 'Einsiedeln', 83, 19, 15361, 99.04, 155.1),
(45, 'Emmen', 23, 28, 30228, 20.33, 1486.9),
(46, 'Flawil', 139, 16, 10505, 11.48, 915.1),
(47, 'Frauenfeld', 31, 20, 25200, 27.37, 920.7),
(48, 'Freienbach', 74, 19, 16196, 13.78, 1175.3),
(49, 'Fribourg', 15, 7, 38829, 9.3, 4175.2),
(50, 'Genève', 2, 8, 201813, 15.93, 12668.7),
(51, 'Gland', 102, 23, 12997, 8.32, 1562.1),
(52, 'Glaris Centre', 108, 9, 12515, 103.7, 120.7),
(53, 'Glaris Nord', 59, 9, 18057, 146.88, 122.9),
(54, 'Gossau', 60, 16, 18055, 27.51, 656.3),
(55, 'Grankpopes', 67, 18, 16985, 26.01, 653),
(56, 'Herisau', 80, 3, 15730, 25.2, 624.2),
(57, 'Hinwil', 132, 26, 11095, 22.3, 497.5),
(58, 'Horgen', 37, 26, 22476, 30.83, 961.2),
(59, 'Horw', 92, 28, 13884, 12.84, 1081.3),
(60, 'Illnau-Effretikon', 69, 26, 16796, 32.92, 510.2),
(61, 'Interlaken', 169, 4, 5673, 4.3, 1319.3),
(62, 'Ittigen', 128, 4, 11388, 4.21, 2705),
(63, 'Kloten', 52, 26, 19086, 19.27, 990.5),
(64, 'Kreuzlingen', 40, 20, 21560, 11.49, 1876.4),
(65, 'Kriens', 27, 28, 27110, 27.34, 991.6),
(66, 'Köniz', 13, 4, 40938, 51.01, 802.5),
(67, 'Küsnacht', 90, 26, 14192, 12.35, 1149.1),
(68, 'La Chaux-de-Fonds', 14, 12, 38965, 55.66, 700.1),
(69, 'La Neuveville', 171, 4, 3693, 6.82, 541.5),
(70, 'La Tour-de-Peilz', 120, 23, 11652, 3.24, 3596.3),
(71, 'Lancy', 22, 8, 31868, 4.77, 6680.9),
(72, 'Langenthal', 81, 4, 15501, 17.26, 898.1),
(73, 'Lausanne', 4, 23, 137810, 41.38, 3330.4),
(74, 'Le Grand-Saconnex', 111, 8, 12130, 4.38, 2769.4),
(75, 'Le Locle', 140, 12, 10433, 23.14, 450.9),
(76, 'Lenzbourg', 152, 5555, 9505, 11.33, 838.9),
(77, 'Liestal', 89, 5, 14234, 18.19, 782.5),
(78, 'Locarno', 75, 21, 16122, 19.27, 836.6),
(79, 'Lucerne', 7, 28, 81592, 29.06, 2807.7),
(80, 'Lugano', 9, 21, 63932, 75.98, 841.4),
(81, 'Lyss', 86, 4, 14706, 14.83, 991.6),
(82, 'Martigny', 61, 24, 17998, 24.97, 720.8),
(83, 'Meilen', 93, 26, 13762, 11.94, 1152.6),
(84, 'Mendrisio', 84, 21, 15110, 32.01, 472),
(85, 'Meyrin', 35, 8, 23611, 9.94, 2375.4),
(86, 'Monthey', 65, 24, 17573, 28.63, 613.8),
(87, 'Montreux', 30, 23, 26629, 33.37, 798),
(88, 'Morat', 161, 7, 8168, 17.41, 469.2),
(89, 'Morges', 77, 23, 15889, 3.85, 4127),
(90, 'Moutier', 164, 4, 7586, 19.58, 387.4),
(91, 'Muri bei Bern', 101, 4, 13037, 7.63, 1708.7),
(92, 'Muttenz', 64, 5, 17737, 16.64, 1065.9),
(93, 'Männedorf', 135, 26, 10830, 4.77, 2270.4),
(94, 'Möhlin', 134, 5555, 10909, 18.79, 580.6),
(95, 'Münchenbuchsee', 146, 4, 10122, 8.82, 1147.6),
(96, 'Münchenstein', 110, 5, 12160, 7.18, 1693.6),
(97, 'Münsingen', 115, 4, 11998, 15.75, 761.8),
(98, 'Neuchâtel', 21, 12, 33772, 18.1, 1865.9),
(99, 'Neuhausen am Rheinfall', 141, 17, 10407, 8, 1300.9),
(100, 'Nyon', 46, 23, 20272, 6.79, 2985.6),
(101, 'Oberwil', 130, 5, 11244, 7.88, 1426.9),
(102, 'Oftringen', 94, 5555, 13483, 12.85, 1049.3),
(103, 'Olten', 58, 18, 18166, 11.49, 1581),
(104, 'Onex', 53, 8, 18869, 2.82, 6691.1),
(105, 'Opfikon', 49, 26, 19599, 5.59, 3506.1),
(106, 'Ostermundigen', 66, 4, 17127, 5.94, 2883.3),
(107, 'Payerne', 153, 23, 9486, 24.19, 392.1),
(108, 'Peseux', 166, 12, 5926, 3.43, 1727.7),
(109, 'Pfäffikon', 117, 26, 11774, 19.55, 602.3),
(110, 'Plan-les-Ouates', 137, 8, 10677, 5.86, 1822),
(111, 'Porrentruy', 165, 11, 6876, 14.75, 466.2),
(112, 'Pratteln', 71, 5, 16315, 10.69, 1526.2),
(113, 'Prilly', 114, 23, 12063, 2.19, 5508.2),
(114, 'Pully', 62, 23, 17972, 5.85, 3072.1),
(115, 'Rapperswil-Jona', 29, 16, 26962, 22.25, 1211.8),
(116, 'Regensdorf', 57, 26, 18191, 14.62, 1244.3),
(117, 'Reinach', 51, 5, 19106, 7, 2729.4),
(118, 'Renens', 45, 23, 20523, 2.96, 6933.4),
(119, 'Rheinfelden', 98, 5555, 13344, 16.03, 832.4),
(120, 'Richterswil', 96, 26, 13352, 7.54, 1770.8),
(121, 'Riehen', 41, 6, 21123, 10.86, 1945),
(122, 'Risch-Rotkreuz', 142, 25, 10355, 14.86, 696.8),
(123, 'Romanshorn', 136, 20, 10803, 8.75, 1234.6),
(124, 'Rorschach', 154, 16, 9408, 1.77, 5315.3),
(125, 'Rüti', 113, 26, 12086, 10.06, 1201.4),
(126, 'Saint-Gall', 8, 16, 75481, 39.39, 1916.2),
(127, 'Saint-Moritz', 170, 10, 5084, 28.69, 177.2),
(128, 'Sarnen', 145, 14, 10233, 73.11, 140),
(129, 'Schaffhouse', 16, 17, 36148, 41.86, 863.5),
(130, 'Schlieren', 54, 26, 18749, 6.6, 2840.8),
(131, 'Schwytz', 85, 19, 14885, 53.25, 279.5),
(132, 'Sierre', 68, 24, 16817, 19.18, 876.8),
(133, 'Sion', 20, 24, 33999, 34.85, 975.6),
(134, 'Soleure', 70, 18, 16697, 6.28, 2658.8),
(135, 'Spiez', 105, 4, 12713, 16.69, 761.7),
(136, 'Spreitenbach', 123, 5555, 11538, 8.6, 1341.6),
(137, 'Stans', 158, 13, 8333, 11.09, 751.4),
(138, 'Steffisburg', 78, 4, 15783, 13.36, 1181.4),
(139, 'Steinhausen', 149, 25, 9735, 5.05, 1927.7),
(140, 'Stäfa', 88, 26, 14291, 8.58, 1665.6),
(141, 'Suhr', 148, 5555, 9960, 10.61, 938.7),
(142, 'Sursee', 151, 28, 9621, 5.86, 1641.8),
(143, 'Thalwil', 63, 26, 17789, 5.5, 3234.4),
(144, 'Thônex', 91, 8, 14064, 3.84, 3662.5),
(145, 'Thoune', 11, 4, 43568, 21.58, 2018.9),
(146, 'Urdorf', 150, 26, 9657, 7.58, 1274),
(147, 'Uster', 19, 26, 34319, 28.53, 1202.9),
(148, 'Uzwil', 103, 16, 12816, 14.5, 883.9),
(149, 'Vernier', 17, 8, 35408, 7.68, 4610.4),
(150, 'Versoix', 95, 8, 13384, 10.51, 1273.5),
(151, 'Vevey', 47, 23, 19780, 2.38, 8310.9),
(152, 'Veyrier', 122, 8, 11578, 6.5, 1781.2),
(153, 'Viège', 163, 24, 7726, 13.17, 586.6),
(154, 'Villars-sur-Glâne', 112, 7, 12128, 5.49, 2209.1),
(155, 'Volketswil', 56, 26, 18582, 14.03, 1324.4),
(156, 'Wallisellen', 76, 26, 15934, 6.43, 2478.1),
(157, 'Weinfelden', 129, 20, 11288, 15.5, 728.3),
(158, 'Wettingen', 44, 5555, 20526, 10.61, 1934.6),
(159, 'Wetzikon', 32, 26, 24640, 16.75, 1471),
(160, 'Wil', 34, 16, 23751, 20.82, 1140.8),
(161, 'Winterthour', 6, 26, 109775, 68.07, 1612.7),
(162, 'Wohlen', 79, 5555, 15765, 12.48, 1263.2),
(163, 'Worb', 126, 4, 11473, 21.1, 543.7),
(164, 'Wädenswil', 39, 26, 21797, 17.39, 1253.4),
(165, 'Yverdon-les-Bains', 24, 23, 29977, 11.28, 2657.5),
(166, 'Zermatt', 168, 24, 5714, 242.69, 23.5),
(167, 'Zofingue', 125, 5555, 11507, 11.09, 1037.6),
(168, 'Zollikofen', 144, 4, 10235, 5.39, 1898.9),
(169, 'Zollikon', 104, 26, 12791, 7.86, 1627.4),
(170, 'Zoug', 25, 25, 29804, 21.61, 1379.2),
(171, 'Zuchwil', 157, 18, 8842, 4.63, 1909.7),
(444444, 'Zurich', 1, 26, 402762, 87.88, 4583.1),
(10000000, '', 43, 2, 20782, 12.33, 1685.5);

-- --------------------------------------------------------

--
-- Structure de la table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cantons`
--
ALTER TABLE `cantons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mon_index` (`name`(11));

--
-- Index pour la table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cities_cantons1_idx` (`cantons_id`);

--
-- Index pour la table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `fk_cities_cantons1` FOREIGN KEY (`cantons_id`) REFERENCES `cantons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
