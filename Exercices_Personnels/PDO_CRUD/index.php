
<?php
require 'database.php';

if(isset($_GET['action']) && $_GET['action']=='delete'){
    $stmt = $conn->prepare('delete from account where username = :username');
    $stmt->bindValue('username', $_GET['username']);
    $stmt->execute();

}

$stmt = $conn->prepare('select * from account');
$stmt->execute();
?>
<a href="register.php">Register</a>
<br>
<br>
<table cellpadding="2" cellspacing="2" border="1">
    <tr>
        <th>Username</th>
        <th>Full Name</th>
        <th>Email</th>
        <th>Option</th>
    </tr>
    <?php while($account = $stmt->fetch(PDO::FETCH_OBJ)) { ?>
        <tr>
            <td><?php echo $account->username; ?></td>
            <td><?php echo $account->fullname; ?></td>
            <td><?php echo $account->email; ?></td>
            <td><a
                    href="index.php?username=<?php echo $account->username; ?>
				&action=delete" onclick="return confirm('Are you sure?')">Delete</a>
                | <a href="edit.php?username=<?php echo $account->username; ?>">Edit</a>
            </td>
        </tr>
    <?php } ?>
</table>

List All Menus
List all level 1 menus and level 2 menus

<?php require_once 'database.php'; ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>jQuery UI Menu - Default functionality</title>
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#menu" ).menu();
        });
    </script>
    <style>
        .ui-menu {
            width: 150px;
        }
    </style>
</head>
<body>
<?php
$stmt = $conn->prepare('select * from menu where parentId is null');
$stmt->execute();
?>

<ul id="menu">
    <?php while($menu1 = $stmt->fetch(PDO::FETCH_OBJ)) { ?>
        <li><?php echo $menu1->name; ?>
            <?php
            $stmt1 = $conn->prepare('select * from menu where parentId = :id');
            $stmt1->bindValue('id', $menu1->id);
            $stmt1->execute();
            ?>
            <?php if($stmt1->rowCount() > 0) { ?>
                <ul>
                    <?php while($menu2 = $stmt1->fetch(PDO::FETCH_OBJ)) { ?>
                        <li><?php echo $menu2->name; ?></li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </li>
    <?php } ?>
</ul>
</body>
</html>
