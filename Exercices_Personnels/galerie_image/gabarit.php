<?php
?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8">
    <title>MonSiteLuca</title>
    <link rel="stylesheet" type="text/css" href="assets/styles.css">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Notepaper Blockquote</title>
    <link rel="stylesheet" href="assets/scss/style.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>

<img class="banderoleCPNV" id="banderoleCPNV" src="image/cpnv.png"/>

<header class="header">
    <h1>
        <a methods="get" class="ButtonMenu2" href="index.php?action=home">Acceuil</a>
        <a methods="get" class="ButtonMenu" href="index.php?action=meteo">Meteo</a>
        <a methods="get" class="ButtonMenu" href="index.php?action=gallery">Galerie d'image</a>

        <a class="cffButton " id="cpnv" href="">CPNV</a>

    </h1>

</header>
<div class="bodyBackground">
    <?= $contenu; ?>
</div>
<script>
    document.getElementById("cpnv").onclick = function () {
        window.open("http://intranet.cpnv.ch/")
    };
</script>


</body>

<footer>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Footer 1 - Left Social/Right Menu -->
    <div class="search-text">
        <div class="container">
            <div class="row text-center">
                <div class="form">
                    <form id="search-form" class="form-search form-horizontal">
                        <input type="text" class="input-search" placeholder="Search">
                        <button type="submit" class="btn-search">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row text-left">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <ul class="list-inline">


                        <a href="#"><i class="fa fa-facebook fa-2x"></i></a>

                        <a href="#"><i class="fa fa-dropbox fa-2x"></i></a>

                        <a href="#"><i class="fa fa-flickr fa-2x"></i></a>

                        <a href="#"><i class="fa fa-github fa-2x"></i></a>

                        <a href="#"><i class="fa fa-linkedin fa-2x"></i></a>

                        <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>

                        <a href="#"><i class="fa fa-tumblr fa-2x"></i></a>
                    </ul>
                </div>
                <div class="footerList2">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="menu inline-block">

                            <li>
                                <a href="#">About</a>

                                <a href="#">Blog</a>

                                <a href="#">Gallery </a>

                                <a href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="copyright">
        <div class="container">

            <div class="row text-center">
                <p>Copyright © 2017 All rights reserved</p>
            </div>

        </div>
    </div>
    <!-- End -->


    </div></footer>
</html>