<?php


include('modeles/modele_posts.php');
if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $update = true;
    $record[] = mysqli_query($db, "SELECT * FROM user_posts WHERE id=$id");

    if (count($record) == 1) {
        $n = mysqli_fetch_array($record[0]);
        $name = $n['name'];
        $post = $n['post'];

    }

}
//ob_start();
?>


<?php if (isset($_SESSION['message'])): ?>
    <div class="msg">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
<?php endif ?>


<?php $results = mysqli_query($db, "SELECT * FROM user_posts"); ?>

    <table>
        <thead>
        <tr>
            <th>Pseudo</th>
            <th>Post</th>

            <th>Editer</th>
            <th>Suprrimer</th>
        </tr>

        </thead>

        <?php while ($row = mysqli_fetch_array($results)) { ?>
            <tr>
                <?php if ($row['name'] == $_SESSION['user']['username']) { ?>
                    <td><?php echo $row['name']; ?></td>
                    <td><textarea disabled><?php echo $row['post']; ?></textarea></td>
                    <td>
                        <a style="vertical-align: center" href="mesPosts.php?edit=<?php echo $row['id']; ?>"
                           class="edit_btn">Edit</a>
                    </td>
                    <td>
                        <a href="server.php?del=<?php echo $row['id']; ?>" class="del_btn">Delete</a>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>


    <form method="post" action="server.php">

        <input type="hidden" name="id" value="<?php echo $id; ?>">

        <div class="input-group">
            <label style="color: #6d3353">Pseudo</label>
            <h4 type="text" name="name" value=""><?php echo $_SESSION['user']['username']; ?></h4>
        </div>
        <div class="input-group">
            <label style="color: #6d3353">Post</label>
            <textarea style="border-block: #0e0e0e" type="text" name="post"><?php echo $post; ?></textarea>
        </div>


        <div class="input-group">

            <?php if ($update == true): ?>
                <button class="btn" type="submit" name="update" style="background: #556B2F;">update</button>
            <?php else: ?>
                <button class="btn" type="submit" name="save">Save</button>
            <?php endif ?>
        </div>
    </form>
    </body>
    </html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";