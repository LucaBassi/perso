<?php

require_once "modeles/modele_users.php";

?>
<html>

<head>
    <title>Dopetrope by HTML5 UP</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
</head>
<body class="centered">
<div id="page-wrapper">

        <!-- Header -->
        <section id="header">

                <h1><a href="index.html">Dopetrope</a></h1>

                <nav id="nav">
                    <ul>
                        <li class="current"><a href="index.html">Home</a></li>
                        <li>
                            <a href="#">Dropdown</a>


                            <?php if (isLoggedIn()) { ?>
                        <li class="current"><a href="index.html">Home</a></li>
                        <li><a style="color: mediumvioletred" methods="post" href="../index.php?action=posts">Posts</a>
                        </li>
                        <li><a style="color: mediumvioletred" methods="post" href="../index.php?action=mesPosts">Mes
                                Posts</a></li>
                        <?php } ?>
                        <?php if (isAdmin()) { ?>
                            <li><a href="../index.php?action=goToUploadImage" style="color: mediumvioletred">Upload
                                    Image</a></li>
                            <li><a href="../index.php?action=adminPage" style="color: mediumvioletred">admin page</a>
                            </li>
                        <?php } ?>
                        <?php if (isset($_SESSION['user']['userEmailAddress'])) : ?>
                            <li><a href="../index.php?action=logout" style="color: red;">logout</a></li>
                        <?php else : ?>
                            <li><a methods="post" href="../index.php?action=login">Login</a></li>
                        <?php endif ?>

                    </ul>

    <!-- logged in user information -->
    <div class="align-right">


        <div style="margin-right: 3%">


            <?php if (isset($_SESSION['user']['userEmailAddress'])) : ?>
                <strong><?php echo $_SESSION['user']['userEmailAddress']; ?></strong>

                <small class="inner">
                    <i style="color: red;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
                    <br>

                    <?php if (isset($_SESSION['user']['userEmailAddress'])) : ?>
                        <h6>Vous êtes connecté : <?= $_SESSION['user']['userEmailAddress']; ?></h6>
                    <?php endif; ?>
                    <?php { ?>
                    <?php } ?>

                </small>
                <div>
                <?php if (isset($_SESSION['user'])) : ?>
                    <?php if ($_SESSION['user']['user_type'] == 'admin') : ?>
                        <small>

                            <h4 style="color: #499249"><a href="create_user.php"> + add user</a></h4>
                        </small>
                    <?php endif ?>
                    </div>
                <?php endif ?>
            <?php else : ?>
                <div style="margin-right:3% ; margin-top: 3%" href=index.php?action=login><h6> Want
                        Login ? Go</h6>
                    <u><a style="margin-right: 3%;margin-bottom: 3%" href="../index.php?action=goLogin"><h3
                                    style="color: #1f6377">! Login !</h3></a></u></div>

            <?php endif ?>

        </div>
    </div>
</div>

<!-- Main -->
<section id="main">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <?php echo $contenu; ?>
            </div>
        </div>
</section>
</div>
</body>

</html>
<footer>





