<?php
/**
 *Bassi Luca
 */
ob_start();
include('functions.php');

if (!isAdmin()) {
    $_SESSION['msg'] = "You must be an Admin to see this page bra";
    header('location: ../login.php');
}


?>

    <h3>Ajouter une personne</h3>

    <div class="inner">

    <form method="post" action="Mon_CRUD_JSON.php" enctype="multipart/form-data">

    <div class="field">

        <div class="row gtr-uniform">
            <div class="col-6 col-12-xsmall">
                <input type="text" name="firstname"text="demo-name" id="demo-name" value="" placeholder="Prenom" />
            </div>
            <div class="col-6 col-12-xsmall">
                <input type="text" name="lastname" id="demo-name" value="" placeholder="Nom" />
            </div>
            <div class="col-2 col-3-xsmall">
                <input type="text" name="age" id="demo-email" value="" placeholder="Age" />
            </div>
            <div class="col-4 col-3-xsmall">
            </div>
            <div class="col-6 col-12-xsmall">
                <input type="text" name="noIdentite" id="demo-name" value="" placeholder="No Identite" />
            </div>
        </div>
        <br>
        </div>
        <div class="row gtr-uniform">
            <div class="col-6 col-12-xsmall">
                <input type="text" name="rue" id="demo-name" value="" placeholder="Rue" />
            </div>
            <div class="col-3 col-6-xsmall">
                <input type="text" name="numero" id="demo-name" value="" placeholder="No" />
            </div>
            <div class="col-3 col-6-xsmall">
            </div>
            <div class="col-6 col-12-xsmall">
                <input type="text" name="city" id="demo-name" value="" placeholder="Ville" />
            </div>
            <div class="col-3 col-6-xsmall">
                <input type="text" name="npa" id="demo-name" value="" placeholder="Npa" />
            </div>
            <div class="col-3 col-6-xsmall">
                <input type="text" name="country" id="demo-name" value="" placeholder="Pays" />
            </div>
            <div class="col-6 col-12-xsmall">
                <input type="text" name="mail" id="demo-name" placeholder="Addresse E-mail" />
            </div>
            <div class="col-6 col-12-xsmall">
                <input type="file" name="uploadfile">
            </div>
        </div>
        <br>

            <!-- Break -->
            <div class="col-12">

                <select name="typeAbonnement" id="demo-category">
                    <option value="">Type d abonnement</option>
                    <option value="Abonnement Genenal">AG</option>
                    <option value="Demi tarif">1/2 tarfif</option>
                    <option value="Mobilis">Mobilis</option>
                    <option value="Autre">Autre</option>
                </select>
            </div><br>




        </br>

            <!-- Break -->
            <div class="col-6 col-12-small">
                <input type="checkbox" id="condition" name="conditions" checked >
                <label for="condition">J'accepte les conditions de voyage et de conduite</label>
            </div>
            <!-- Break -->
            <div class="col-10">
                <textarea name="message" id="demo-message" placeholder="Enter your message" rows=2-small></textarea>
            </div>
        <br>
            <!-- Break -->
            <div class="col-12">
                <ul class="actions">
                    <li><input type="submit" value="Send Request" class="primary" /></li>
                    <li><input type="reset" value="Reset" /></li>
                </ul>
            </div>
        </div>



    <p>Take a look at -> fichier json brut  <a href="../data/mydata.json" target="_blank">Take a look at -> fichier json brut</a></p>


    <script>
        function toggle(checked) {
            var elm = document.getElementById('conditions');
            if (checked != elm.checked) {
                elm.click();
            }
        }
    </script>
<?php
$contenu = ob_get_clean();
require "gabarit.php";

?>