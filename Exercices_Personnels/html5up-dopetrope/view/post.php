<?php ob_start();
$rows = 0; // Column count
?>


    <article>

            <h2> Nos snows</h2>
            <div class="yox-view">

                <?php foreach ($posts as $post): ?>
                    <?php $rows++; ?>
                    <?php if ($rows=4) : // tests to have 4 items / line ?>
                        <div class="row-fluid">
                        <ul class="thumbnails">

                        <?php $rows=0;?>
                    <?php endif ?>
                    <li class="span3">
                    <div class="row">
                    <div class="col-8 col-12-small">
                        <section class="box">
                            <a href="#" class="image featured"><img src="images/pic08.jpg" alt="" /></a>
                            <header>
                                <h3>Magna tempus consequat</h3>
                                <p>Posted 45 minutes ago</p>
                                <p> <?= $post['name'] ?></p>
                                <a><?= $post['post'] ?></a>

                            </header>
                            <p><?= $post['post'] ?></p>
                            <p>Lorem ipsum dolor sit amet sit veroeros sed et blandit consequat sed veroeros lorem et blandit adipiscing feugiat phasellus tempus hendrerit, tortor vitae mattis tempor, sapien sem feugiat sapien, id suscipit magna felis nec elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos lorem ipsum dolor sit amet.</p>
                            <footer>
                                <ul class="actions">
                                    <li><a href="#" class="button icon solid fa-file-alt">Continue Reading</a></li>
                                    <li><a href="#" class="button alt icon solid fa-comment">33 comments</a></li>
                                </ul>
                            </footer>
                        </section>
                    </div>
                    <?php if ($rows=4) :?>
                        </ul>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>

            </div>

    </article>
    <hr/>
<?php
$contenu = ob_get_clean();
require_once "gabarit.php";
?>