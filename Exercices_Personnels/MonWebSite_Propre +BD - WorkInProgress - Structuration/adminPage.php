<?php

include('modeles/modele_users.php');

if (!isLoggedIn()) {
    $_SESSION['msg'] = " to see this page ->You must log in first ";
    header('location: login.php');

}

ob_start();
?>

    <div class="inner">

        <section >

            <div class="inner">
                <section class="inner">

                    <table>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nom</th>
                            <th>Addresse mail</th>
                            <th>Type</th>
                            <th>Del</th>
                        </tr>
                        </thead>
                        <?php $userResults = mysqli_query($db, "SELECT * FROM users"); ?>
                        <?php while ($row = mysqli_fetch_array($userResults)) {
                            $nbCol=0;
                            $nbCol++;
                        ?>
                            <tr>
                                <td><?php echo $nbCol++; ?></td>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $row['email']; ?></td>
                                <td><?php echo $row['user_type']; ?></td>
                                <td>
                                <?php if ($row['user_type']=="user"){ ?>
                               <a href="modeles/modele_users.php?del_user=<?php echo $row['id_user']; ?>" disabled >Delete</a>
                                <?php } ?>
                                </td>
                            </tr>
                        <?php
                       } ?>
                    </table>
                </section>
            </div>
        </section>
    </div>


<?php
$contenu = ob_get_clean();
require "gabarit.php";

