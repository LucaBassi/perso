<?php


function home()
{
    require 'home.php';
}


function posts(){
    require_once 'Modele.php';
    try {
        $billets = getBillets();
        require 'vuePosts.php';
    }
    catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

function myImages(){
    require_once 'Modele.php';
    try {
        $images = getImages();
        require 'userPage.php';
    }
    catch (Exception $e) {
        $msgErreur = $e->getMessage();
        require 'vueErreur.php';
    }

}

if (isset($_POST['register_btn'])) {
    register();
}

// call the login() function if register_btn is clicked
function goLogin(){
    require_once 'Modele.php';

    login();

}

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['user']);
    header("location: home.php");
}

