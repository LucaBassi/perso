
<?php
include "modeles/modele_users.php";
ob_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Registration system PHP and MySQL</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<div class="header">
		<h2>Login</h2>
	</div>
	
	<form class="inner" method="post" action="login.php">

		<?php
		    echo display_error();
		?>


        <div class="header">
            <h3 style="color: red">
		<?php
            if (isset($_SESSION['msg'])){
		    echo $_SESSION['msg'];
        }; ?>
            </h3>
        </div>

		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" >
		</div>
		<div class="input-group">
			<label>Password</label>
			<input type="password" name="password">
		</div>
        <br>
		<div class="input-group">
			<button type="submit" class="btn" name="login_btn">Login</button>
		</div>
        <br>
		<p>
        <h4>Not yet a member?</h4> <a href="register.php"><h6>Sign up</h6></a>
		</p>
	</form>


</body>
</html>
<?php
$contenu=ob_get_clean();
require "gabarit.php";
