<?php

include 'functions.php';
include('server.php');
	if (isset($_GET['edit'])) {
		$id = $_GET['edit'];
		$update = true;
		$record[] = mysqli_query($db, "SELECT * FROM user_posts WHERE id=$id");

		if (count($record)==1) {
			$n = mysqli_fetch_array($record[0]);
			$name = $n['name'];
			$post = $n['post'];

		}

	}
	ob_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>CRUD: CReate, Update, Delete PHP MySQL </title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php if (isset($_SESSION['message'])): ?>
		<div class="msg">
			<?php 
				echo $_SESSION['message']; 
				unset($_SESSION['message']);
			?>
		</div>
	<?php endif ?>

<?php $results = mysqli_query($db, "SELECT * FROM user_posts"); ?>

<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Address</th>

			<th colspan="2">Action</th>
		</tr>
	</thead>
	
	<?php while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['post']; ?></td>
			<td>
				<a href="post_index.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >Edit</a>
			</td>
			<td>
				<a href="posts/server.php?del=<?php echo $row['id']; ?>" class="del_btn">Delete</a>
			</td>
		</tr>
	<?php } ?>
</table>
	


<form method="post" action="posts/server.php" >

	<input type="hidden" name="id" value="<?php echo $id; ?>">

	<div class="input-group">
		<label >Name <h4> <?php echo $_SESSION['user']['username']; ?></h4></label>
	</div>
	<div class="input-group">
		<label>Post</label>
		<input type="text" name="post" value="<?php echo $post; ?>">
	</div>

	<div class="input-group">

		<?php if ($update == true): ?>
			<button class="btn" type="submit" name="update" style="background: #556B2F;" >update</button>
		<?php else: ?>
			<button class="btn" type="submit" name="save" >Save</button>
		<?php endif ?>
	</div>
</form>
</body>
</html>
<?php
$contenu =ob_get_clean();
require "gabarit.php";