<?php

$host = "localhost";    /* Host name */
$user = "root";         /* User */
$password = "";         /* Password */
$dbname = "DB_Images";   /* Database name */

// Create connection
$con = mysqli_connect($host, $user, $password,$dbname) or die(mysqli_error());
$ret = mysqli_select_db($con,$dbname) or die(mysqli_error());
// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}

