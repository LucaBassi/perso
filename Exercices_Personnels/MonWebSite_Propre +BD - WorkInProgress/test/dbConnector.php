<?php
function openDBConnexion(){
    $dbConnexion = 0;

    $sqlDriver = "mysql";
    $dbHostname = "localhost";
    $dbPort = 3330;
    $dbCharset = "utf8";
    $dbName = "db_images";

    $dbUsername = "root";
    $dbPassword = "root";

    $dbInfo = $sqlDriver.":host=".$dbHostname.";dbname=".$dbName.";port=".$dbPort.";charset=".$dbCharset;

    try
    {
        $dbConnexion = new PDO($dbInfo, $dbUsername, $dbPassword);
    }
    catch (PDOException $exception)
    {
        echo "Connexion failed; ".$exception->getMessage();
    }

    return $dbConnexion;
}