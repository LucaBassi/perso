<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : 151_2019_ForStudents
 * Created  : 05.02.2019 - 18:40
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */

function home()
{
    require "view/home.php";
}

function login($loginRequest)
{
    if(isset($loginRequest["inputUserEmailAddress"]) && isset($loginRequest["inputUserPassword"]))
    {
        $userEmail = $loginRequest["inputUserEmailAddress"];

      //  $userPassword = $loginRequest["inputUserPassword"];

        //$userPassword = password_hash($loginRequest["inputUserPassword"], PASSWORD_DEFAULT);

        $userPassword = hash("sha256", $loginRequest["inputUserPassword"]);

        require "model/userManagement.php";
        if (isLoginCorrect($userEmail, $userPassword))
        {
            $_SESSION["userEmail"] = $userEmail;
            $_GET["action"] = "home";
            require "view/home.php";
        }
        else
        {
            $_SESSION["userEmail"] = $userEmail;
            $_GET["action"] = "login";
            require "view/login.php";
        }

    }
    else
    {
        $_GET["action"] = "login";
        require "view/login.php";
    }
}


function logout()
{
    $_SESSION = array();
    session_destroy();
    $_GET["action"] = "home";
    require "view/home.php";
}

/**
 * Function : Displays all the snows
 * Author : SMR
 * Input : none
 * Output : none
 */
function displaySnows()
{
    require "model/snowsManager.php";
    $snowsResults = getSnows();
    $_GET['action']="displaySnows";
    require "view/snowsUser.php";
}



function register($registerRequest){
    $error = "";
    $errors = 0;


    $isPasswordEqual = false;


    //Check if all field a filled
    if(isset($registerRequest["inputUserEmailAddress"]) && isset($registerRequest["inputUserPassword"]) && isset($registerRequest["inputUserPasswordConfirm"]))
    {
        //Put every info in a var

        $userEmail = $registerRequest["inputUserEmailAddress"];
        $userPassword = $registerRequest["inputUserPassword"];
        $userPasswordConfirm = $registerRequest["inputUserPasswordConfirm"];

        //Check in the DB if the Email is used
        require "model/userManagement.php";
        $isEmailUsed = checkEmailDB($userEmail);
        if($isEmailUsed == true){
            $error = "Email already in use, try <a href='index_image.php?action=login'>Login</a>";
            $errors += 1;
        }
        //Check if password and confirm password are equal
        if($userPassword != $userPasswordConfirm) {
            $error = "Password confirm isn't equal to Password";
            $errors += 1;
        }
        //if everything is OK
        if($errors == 0 && $error == ""){
            $userPasswordConfirm = "";
            $pswHash = password_hash($userPassword, PASSWORD_DEFAULT);
            $userPassword = "";
            //Add to database
            registerDB($userEmail, $pswHash);
            $_SESSION["userEmail"] = $userEmail;
            $_GET['action'] = "home";
            require "view/home.php";
        }
    }
    else
    {
        require "view/Register.php";
    }
}