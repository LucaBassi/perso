<?php
include "modeles/modele_users.php";
if (isset($_GET)) {
    extract($_GET);
}
ob_start();
?>


    <!doctype html>

    <html>
    <head >
        <meta charset="utf-8">
    </head>
    <body>

    <section class="inner" >
        <section id="one">
            <div class="inner">
                <img style="vertical-align: center;display: flex; max-width:15%;position: center;margin: auto" src="data/CFF/Logo_CFF.png" alt="" />
            </div>
        </section>
    </section>

    <br>
    <section class="inner">

        <table  class="alt">
            <thead>
            <tr>
                <th></th> <th></th> <th></th>
                <th > <button style="max-width: 100%; height: auto" type="button" class="button small" id="showImage" onclick="showImageCffAller()">Cff -> Aller</button>
                    <br>
                </th>
                <p><a href="https://www.sbb.ch/fr/acheter/pages/fahrplan/fahrplan.xhtml" target="_blank">Lien direct vers cff.ch</a></p>

                <th style="text-align: right" >  <p><button style="max-width: 100% ;height: auto"  type="button" class="button small" id="showImage" onclick="showImageCffRetour()">Cff <- Retour</button></th>
                <th></th> <th></th> <th></th>
            </tr>
            </thead>
        </table>

<div>
    <section class="inner">
    <form  method="get" action="transports.php" >




            <table>

                <tbody>
                <tr>
                    <td>
                        <img style=" margin-left: 25%; width: 50% ;height: auto;" id="cff_aller" src="">
                    </td>

                </tr>
                <tr>
                    <td>
                        <img style="margin-left:25%; width: 50% ;height: auto" id="cff_retour" src="">
                    </td>

                </tbody>
            </table>
        </form>
    </div>
</section>

    <section class="inner">
        <section id="one">
            <div class="inner">
                <img style="vertical-align: center;display: flex; max-width:15%;position: center;margin: auto" src="data/SNCF/SNCF_logo.png" alt="" />
                <p><a href="https://www.sncf.com/fr/itineraire-reservation/itineraire/liste-resultats/details?uic1=OCE85010082&coordX1=6.14316&coordY1=46.21056&uic2=75056&coordX2=2.35085&coordY2=48.8569&date=1588664374&when=&id=-746171852&typeDepart=ZONE_ARRET&typeArrivee=COMMUNE&label1=Gen%C3%A8ve%20Cornavin&label2=Paris%20(Toutes%20gares)&listeCodesMode=0,1,2,3,4,5,6,7,8,9,10&origineCP=&destinationCP=75004" target="_blank">Lien direct vers SNCF ---> allé</a></p>
                <p><a href="https://www.sncf.com/fr/itineraire-reservation/itineraire/liste-resultats/details?uic1=75056&coordX1=2.35085&coordY1=48.8569&uic2=OCE85010082&coordX2=6.14316&coordY2=46.21056&date=1588932034&when=arrive-at&id=-648194216&typeDepart=COMMUNE&typeArrivee=ZONE_ARRET&label1=Paris%20(Toutes%20gares)&label2=Gen%C3%A8ve%20Cornavin&listeCodesMode=0,1,2,3,4,5,6,7,8,9,10&origineCP=75004&destinationCP=" target="_blank">Lien direct vers SNCF ---> Retour</a></p>

            </div>
        </section>
    </section>

    <section class="inner">
        <table  class="alt">

            <thead>
            <tr>
                <th></th> <th></th> <th></th>
                <th > <button style="max-width: 100%; height: auto" type="button" class="button small" id="showImage" onclick="showImageSncfAller()">Sncf -> Aller</button></th>
                <th style="text-align: right" >  <p><button style="max-width: 100% ;height: auto"  type="button" class="button small" id="showImage" onclick="showImageSncfRetour()">Sncf <- Retour</button></th>
                <th></th> <th></th> <th></th>
            </tr>
            </thead>
        </table>

        <div>

        <form  method="get" action="transports.php" >




            <table>
                <tbody>
                <tr>
                    <td>
                        <img style=" margin-left: 25%; width: 50% ;height: auto;" id="sncf_aller" src="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img style="margin-left:25%; width: 50% ;height: auto" id="sncf_retour" src="">
                    </td>

                </tbody>
            </table>
        </form>
    </section>

<section class="inner">
    <div>
        <h4>Resume du voyage</h4>
        <div>
            <label>
<textarea style="max-width: 200% ; height : 50%"  disabled>

    ALLER:
    Depart ---- Ste-Croix ---- 05.05.2020 ---- 8h06 ---- Voie 1


    Retour ---- Ste-Croix ---- 08.05.2020 ---- 16h21 ---- Voie B3

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    RETOUR:
    Depart ---- Ste-Croix ---- 05.05.2020 ---- 8h06 ---- Voie 1


    Retour ---- Ste-Croix ---- 08.05.2020 ---- 16h21 ---- Voie B3
</textarea>
            </label>
        </div>
    </div>
</section>
<section class="inner">
    <div>
    <table>

    <p><a href="data/mydata.json" target="_blank">Jetez un oeil -> fichier json brut - Personnes</a>
    <p><a href="data/transport_data.json" target="_blank">Jetez un oeil -> fichier json brut - Transports</a></p>

    </table>
    </div>
</section>

<section class="inner">
    <div>

        <?php include "connections.php";?>

    </div>
</section>

</body>

<script>

    function showImageCffAller() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);
                    document.getElementById("cff_aller").src=(Object(myObj.transports[0].cff_aller));
                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/transport_data.json", true);
        xmlhttp.send();
    }





    function showImageCffRetour() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);

                    document.getElementById("cff_retour").src=(Object(myObj.transports[0].cff_retour));

                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/transport_data.json", true);
        xmlhttp.send();
    }




    function showImageSncfAller() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);
                    document.getElementById("sncf_aller").src=(Object(myObj.transports[1].sncf_aller));
                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/transport_data.json", true);
        xmlhttp.send();
    }





    function showImageSncfRetour() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //met le contenu du ficjier json dans un objet javascript
                var myObj = JSON.parse(this.responseText);

                    document.getElementById("sncf_retour").src=(Object(myObj.transports[1].sncf_retour));

                }
        };
        //chemin du fichier json
        xmlhttp.open("GET", "data/transport_data.json", true);
        xmlhttp.send();
    }










//window.onload = showImage() ;
</script>





    <?php
    $contenu = ob_get_clean();
    require "gabarit.php";

    ?>





