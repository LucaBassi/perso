<?php

$date = getdate();
//$hour=$date['hours'];
$hours = $date['hours'];
$minutes = $date['minutes'];
$seconds = $date['seconds'];

$completDate = date('Y:m:d');

$yearDate = $date['year'];
$monthDate = $date['month'];
$numDayDate = $date['mday'];

if (isset($date['day'])) {
    $dayDate = $date['day'];
}

$wday[1] = 'Lundi';
$wday[2] = 'Mardi';
$wday[3] = 'Mercredi';
$wday[4] = 'Jeudi';
$wday[5] = 'Vendredi';
$wday[6] = 'Samedi';
$wday[0] = 'Dimanche';
$today = $wday[$date['wday']];


$data = getData(); // Initialize data with fixed values;
// This function simply returns some hardcoded data
function getData(){
    return json_decode('[
{
    "personnes": [
      {
        "id": 1,
        "date": [
          {
            "jour": "Dimanche",
            "mois": "December",
            "noJour": 03,
            "noMois": 12,
            "annee": 2019,
            "completDate": "2019:12:03"
          }
        ],
        "heure": [
          {
            "heure": 10,
            "minutes": 53,
            "secondes": 47

          }
        ],
        "description": "blablabla",
        "nomPhoto": "1-a-a-mozilla-firefox-wallpaper.jpg",
        "photo": "uploads\/1-a-a-mozilla-firefox-wallpaper.jpg"
      },
      {
        "id": 2,
        "date": [
          {
            "jour": "Dimanche",
            "mois": "December",
            "noJour": 22,
            "noMois": 04,
            "annee": 2022,
            "completDate": "2022:04:22"
          }
        ],
        "heure": [
          {
            "heure": 10,
            "minutes": 54,
            "secondes": 3

          }
        ],
        "description": "blobloblblo",
        "nomPhoto": "2-b-b-FireSphere.jpg",
        "photo": "uploads\/2-b-b-FireSphere.jpg"
      },
      {
        "id": 3,
        "date": [
          {
            "jour": "Dimanche",
            "mois": "December",
            "noJour": 22,
            "noMois": 12,
            "annee": 2019,
            "completDate": "2019:12:22"
          }
        ],
        "heure": [
          {
            "heure": 10,
            "minutes": 53,
            "secondes": 47

          }
        ],
        "description": "bliblbiblibli",
        "nomPhoto": "3-c-c-firefoxAurora.jpg",
        "photo": "uploads\/3-c-c-firefoxAurora.jpg"
      }
    ]
  }
]', true);

}

// ============== Load or create data ================

$dataDirectory = "data";
$dataFileName = 'mydata.json';


if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
    {
    $data = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
}else
{
    if (!file_exists($dataDirectory)) // Check if data directory exists
    {
        mkdir($dataDirectory); // if not create it
    }
    $data = getData(); // Initialize data with fixed values

    copy('Image/hardCoded/1-a-a-mozilla-firefox-wallpaper.jpg', "uploads/1-a-a-mozilla-firefox-wallpaper.jpg");
    copy('Image/hardCoded/2-b-b-FireSphere.jpg', "uploads/2-b-b-FireSphere.jpg");
    copy('Image/hardCoded/3-c-c-firefoxAurora.jpg', "uploads/3-c-c-firefoxAurora.jpg");


    file_put_contents("$dataDirectory/$dataFileName", json_encode($data));


}






// ============== Process commands from GET parameters ================
extract($_GET); // possible variables created:  $init, $create, $update, $delete, $index, $firstname, $lastname, $hobby
//                              $mail,$noIdentite,$rue,$numero,$country,$city,$npa, photoPersonne;

extract($_POST);
// --- 1. init
/*
if (isset($init)) // reinitialise data
{
    $data = getData(); // Initialize data with fixed values
    echo "Données réinitialisées";
}
*/
// --- 2. delete
/*
if (isset($delete)) // delete the person of the array who is at index "$index"
{
    echo "Suppression de " . $data[$index]['Firstname'] . "<br>";

    for ($i = $index; $i < count($data) - 1; $i++) // shift all elements beyond the one we must delete
    {
        $data[$i] = $data[$i + 1];
    }
    unset($data[$i]); // destroy the last one
}
*/


if (isset($_POST['submitform'])) {
    $json_decode_str = $data;
    $my_index = 0;
    foreach ($json_decode_str['0']['personnes'] as $personne) {


        if ($my_index == 1) {
            //  echo $personne['firstname'];
        }

        $my_index++;
    }

    //  echo $my_index;

    /*for ($i = $index; $i < count($data[0]) - 1; $i++) // shift all elements beyond the one we must delete
    {
        $data[$index]++;
        $data[$i] = $data[$index];
        $index = $i;
        $index++;
        //  $j=$index;
    }*/

    //echo  $friend = [$my_index]['personnes'];
    // echo "Modification de ".$friend['firstname'];

    $friend['id'] = $my_index;

    $friend['date']['0']['jour'] = $today;

    $friend['date']['0']['noJour'] = $numDayDate;

    $friend['date']['0']['mois'] = $monthDate;

    $friend['date']['0']['annee'] = $yearDate;

    $friend['date']['0']['completDate'] = $completDate;

    $friend['heure']['0']['heure'] = $hours;

    $friend['heure']['0']['minutes'] = $minutes;

    $friend['heure']['0']['secondes'] = $seconds;

    if (isset($message)) // a hobby was given in the querystring
    {
        $friend['description'] = $message;
    }


    $dir = "uploads/";
    $image = $_FILES['uploadfile']['name'];
    $temp_name = $_FILES['uploadfile']['tmp_name'];

    if ($image != "") {
        if (file_exists($dir . $image)) {
            $image = time() . '_' . $image;
        }

        //rename image like : "index-firstname-lastname-imagename.extension" and add is relative id (from my_index out of foreach)
        //$extensionFile=$image['extension'];
        $image = $my_index . '-' . $friend['firstname'] . '-' . $friend['lastname'] . '-' . $image;

        $fdir = $dir . $image;
        move_uploaded_file($temp_name, $fdir);

        $friend['nomPhoto'] = $image;

        $friend['photo'] = $fdir;

        echo "File Uploaded Suucessfully ";
        header('location: gallery.php');
    }

}



$data[0]['personnes'][] = $friend; // save


file_put_contents("$dataDirectory/$dataFileName", json_encode($data));

