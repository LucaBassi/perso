MonWebSite_Propre+BD
---
Cette application à besoin des configuration suivantes pour fonctionner :
---
Dans le fichier "php.ini" Decommenter les extensions suivantes :

* Mysqli
* OpenSSL
___

Deployez un serveur Xampp avec les services suivants :
* Appache
* MySql
___

Dans le le fichier "my.ini" du service MySql :
* max_allowed_packet=128M
* innodb_log_file_size=128M
___

Demarrer une session serveur PhpMyAdmin en localhost avec la configuration suivante:
* login :     "root"
* psw :        ""
___

Creez 3 bases de donnee suivants avec les noms suivants : 
* crud
* multi_login
* images
___

Lancez les script suivants dans le dossier SQL :
* images.sql
* multi_login.sql
* user_posts.sql
___

___
**Perso**
a faire :

-les admin doivent pouvoir gerer les user -> CRUD

-une page ou les users voient tous leurs posts

-une galerie photo avec nom des users 