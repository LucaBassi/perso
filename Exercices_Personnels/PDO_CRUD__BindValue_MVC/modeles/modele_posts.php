<?php

// Effectue la connexion à la BDD
// Instancie et renvoie l'objet PDO associé

/*
function getBdd()
{
    $bdd = new PDO('mysql:host=localhost;dbname=multi_login;charset=utf8',
        'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    return $bdd;

}*/


// Renvoie la liste de tous les billets, triés par identifiant décroissant
function getPosts()
{
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    $posts = $bdd->query('SELECT * FROM user_posts order by date desc ,time DESC ');
    unset($bdd);
    return $posts;

}

function getUserPosts()
{


    $user = $_SESSION["user"]["userEmailAddress"];
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    $userPosts = $bdd->query("SELECT id,name,post FROM user_posts where name = '$user' ");
    $id = 0;

    return $userPosts;
}


function getAPost($id)
{
    $user = $_SESSION["user"]["userEmailAddress"];
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    $post = $bdd->prepare("SELECT id,name,post,image FROM user_posts where id = '$id' ");
    $post->execute(array($id));
    if ($post->rowCount() == 1)
        return $post->fetch();  // Accès à la première ligne de résultat
    else
        throw new Exception("Aucun billet ne correspond à l'identifiant '$id'");

}


function insertAPost($autor, $post,$file, $user_id, $date, $time)
{

    $strSeparator = '\'';

    $query = 'INSERT INTO user_posts (name,post,image,date,time,user_id_post) VALUES (' . $strSeparator . $autor . $strSeparator . ',' . $strSeparator . $post . $strSeparator . ',' . $strSeparator . $file . $strSeparator . ',' . $strSeparator . $date . $strSeparator . ',' . $strSeparator . $time . $strSeparator . ',' . $strSeparator . $user_id . $strSeparator . ' ) ';

    require_once 'modeles/connector.php';
    $queryResult = executeQueryInsert($query);
    if ($queryResult) {
        $result = $queryResult;
    }
    unset($queryResult);
    return $result;
}


function updatePost($id, $post)
{
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    $updatePost = $bdd->query("UPDATE user_posts SET post='$post' WHERE id=$id");

    /*
        $id = $_POST['id'];
        $name = $_SESSION['user']['username'];
        $post = $_POST['post'];

        mysqli_query($db, "UPDATE user_posts SET name='$name', post='$post' WHERE id=$id");*/
    $_SESSION['message'] = "Your post is updated!";
}


function getAllComments($idComment)
{
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    $getComments = $bdd->query("SELECT id,date,autor,comment FROM comments where post_id_comment = $idComment  order by date DESC , time DESC  ");
    // $getComments->execute(array($idComment));
    $youpi = $getComments;
    return $getComments;

    /*    else {
            throw new Exception("Aucun billet ne correspond à l'identifiant '$idComment'");
        }*/
    // return $getComments;
}


function countComments()
{


    $strSeparator = "'";

    $count = '   Select user_posts.id,comments.comment, Count(comments.post_id_comment) From user_posts Inner join comments on comments.post_id_comment Where user_posts.id = comments.post_id_comment Group By user_posts.post ';

    require_once 'modeles/connector.php';
    $count = executeQuerySelect($count);

    return $count;
}


function addAComment($comment, $idComment,$user_id, $autor, $date, $time)
{
    require_once 'modeles/connector.php';
    $bdd = getBdd();
    // $strSeparator = '\'';
    $result = false;

    $strSeparator = '\'';

    $newComment = 'INSERT INTO comments (comment, post_id_comment,comments.user_id_comment,autor, date,time) VALUES (' . $strSeparator . $comment . $strSeparator . ',' . $strSeparator . $idComment . $strSeparator .',' . $strSeparator . $user_id . $strSeparator . ',' . $strSeparator . $autor . $strSeparator . ',' . $strSeparator . $date . $strSeparator . ',' . $strSeparator . $time . $strSeparator . ')';
    require_once 'modeles/connector.php';
    $queryResult = executeQueryInsert($newComment);
    if ($queryResult) {
        $result = $queryResult;
    }
    return $result;

}



