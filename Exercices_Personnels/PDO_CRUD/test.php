
Search Page
<html>
<head>
    <title>Search AutoComplete</title>
</head>
<script type="text/javascript" src="js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.js"></script>
<link href="css/themes/base/jquery.ui.all.css" type="text/css" rel="stylesheet">
<script type="text/javascript">

    $(document).ready(function(){
        $('#productName').autocomplete({
            source: 'search.php'
        });
    });

</script>
<body>

<form>
    Product Name <input type="text" id="productName">
</form>

</body>
</html>