Questions pour M.Glassey:
---

##### Stucture MVC
* Dans le "controler" faut-il avoir le moins de données extraites de la BD ou peut-on trier des données dans le "controler" ou le tri se fait dans le modele    


##### Stockage des données
- Dans quel cas stocker images en blob dans DB ou dans un fichier ?
- avantages/inc

- (BD unique pour toutes les images (users et posts) ?)


##### BindValue
* Possibilité de "BindValue" dans un fichier séparé la la clase de connexion a la BD


- critiques sur :

1. Demande d'aide apprpriée ou non 
2. Formulation de la demande 
3. Infos données, qualité, présision, liens
4. Le fait de demander de l aide sur un projet non structuré
5. Qualité  du ReadMe
6. 
7. 
8. 
9. 
10. toutes autres possibles


##### "Mise en production"

- Existe-il un moyen de "cacher le fichier de connexion" à la BD avec les noms d'utilisateurs et les MDP

- J'ai vu quelque chose sur le fichier "htdocs" est-ce correct ?
- Est- une bonne solution ?
- Que manque-il pour pouvoir mettre un projet (comme Snows par exemple) en ligne ?
(MVC bien fait et MDPs cryptés)