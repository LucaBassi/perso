<?php



function checkLogin($userEmailAddress, $userPsw)
{
    $result = false;

    $strSeparator = '\'';
    $loginQuery = 'SELECT password FROM users WHERE username = ' . $strSeparator . $userEmailAddress . $strSeparator;

    require_once 'modeles/connector.php';
    $queryResult = executeQuerySelect($loginQuery);

    if (count($queryResult) == 1) {
        $userHashPsw = $queryResult[0]['password'];
        $hashPasswordDebug = password_hash($userPsw, PASSWORD_DEFAULT);
        $result = password_verify($userPsw, $userHashPsw);
    }
    return $result;
}

/**
 * This function is designed to register a new account
 * @param $userEmailAddress
 * @param $userPsw
 * @return bool|null
 */
function registerNewAccount($userEmailAddress, $userPsw)
{
    $result = false;

    $strSeparator = '\'';

    $userHashPsw = password_hash($userPsw, PASSWORD_ARGON2I);

    $registerQuery = 'INSERT INTO users (username, password) VALUES (' . $strSeparator . $userEmailAddress . $strSeparator . ',' . $strSeparator . $userHashPsw . $strSeparator . ')';

    require_once 'modeles/connector.php';
    $queryResult = executeQueryInsert($registerQuery);
    if ($queryResult) {
        $result = $queryResult;
    }
    return $result;
}

/**
 * This function is designed to get the type of user
 * For the webapp, it will adapt the behavior of the GUI
 * @param $userEmailAddress
 * @return int (1 = customer ; 2 = seller)
 */
function getUserType($userEmailAddress)
{
  //  $result = 1;//we fix the result to 1 -> customer

    $strSeparator = "'";

    $getUserTypeQuery = 'SELECT user_type FROM users WHERE username = ' . $strSeparator . $userEmailAddress . $strSeparator;

    require_once 'modeles/connector.php';
    $queryResult = executeQuerySelect($getUserTypeQuery);

    if (count($queryResult) == 1) {
        $result = $queryResult[0]['user_type'];
    }
    return $result;
}

































/*


function getUserPosts()
{


    $username = $_SESSION["user"]["username"];
    require_once "controler/connector.php";
    $bdd = getBdd();
    $usersPosts = $bdd->query("SELECT name,post FROM user_posts where name = '$username' " );*/
  //  $id = 0;

    //return $usersPosts;

// connect to database
/*
$db = mysqli_connect('localhost', 'root', '', 'multi_login');*/

// variable declaration
/*$username = "";
$email    = "";
$errors   = array();

// call the register() function if register_btn is clicked
if (isset($_POST['register_btn'])) {
    register();
}

// call the login() function if register_btn is clicked
if (isset($_POST['login_btn'])) {
    login();
}

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['user']);
    header("location: home.php");
}*/

// REGISTER USER
/*
function register(){
    global $db, $errors;

    // receive all input values from the form
    $username    =  e($_POST['username']);
    $email       =  e($_POST['email']);
    $password_1  =  e($_POST['password_1']);
    $password_2  =  e($_POST['password_2']);

    // form validation: ensure that the form is correctly filled
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password_1)) {
        array_push($errors, "Password is required");
    }
    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }

    // register user if there are no errors in the form
    if (count($errors) == 0) {
        $password = md5($password_1);//encrypt the password before saving in the database

        if (isset($_POST['user_type'])) {
            $user_type = e($_POST['user_type']);
            $query = "INSERT INTO users (username, email, user_type, password) 
						  VALUES('$username', '$email', '$user_type', '$password')";
            mysqli_query($db, $query);
            $_SESSION['success']  = "New user successfully created!!";
            header('location: home.php');
        }else{
            $query = "INSERT INTO users (username, email, user_type, password) 
						  VALUES('$username', '$email', 'user', '$password')";
            mysqli_query($db, $query);

            // get id of the created user
            $logged_in_user_id = mysqli_insert_id($db);

            $_SESSION['user'] = getUserById($logged_in_user_id); // put logged in user in session
            $_SESSION['success']  = "You are now logged in";
            header('location: home.php');
        }

    }

}

// return user array from their id
function getUserById($id){
    global $db;
    $query = "SELECT * FROM users WHERE id=" . $id;
    $result = mysqli_query($db, $query);

    $user = mysqli_fetch_assoc($result);
    return $user;
}*/

// LOGIN USER
/*
function login(){



    $username = $_SESSION["user"]["username"];
    require_once "controler/connector.php";
    $bdd = getBdd();
    $login= $bdd->query("SELECT name,post FROM user_posts where name = '$username' " );

        $result = false;

        $strSeparator = '\'';
        $password=$_POST["password"];
        $loginQuery = "SELECT * FROM users WHERE username= '$username'AND password = '$password' LIMIT 1";

        require_once 'model/dbConnector.php';
        $queryResult = executeQuerySelect($loginQuery);

        if (count($queryResult) == 1)
        {
            $userHashPsw = $queryResult[0]['userHashPsw'];
            $hashPasswordDebug = password_hash($userPsw, PASSWORD_DEFAULT);
            $result = password_verify($userPsw, $userHashPsw);
        }
        return $result;
    }
    $password = md5($password);

    $query = "SELECT * FROM users WHERE username='$username' AND password='$password' LIMIT 1";
    $results = mysqli_query($db, $query);

    if (mysqli_num_rows($results) == 1) { // user found
        // check if user is admin or user
        $logged_in_user = mysqli_fetch_assoc($results);
        if ($logged_in_user['user_type'] == 'admin') {

            $_SESSION['user'] = $logged_in_user;
            $_SESSION['success'] = "You are now logged in";


            /*  header('location: home.php');*/
     //       isLoggedIn();
            //require_once "controler/users_controler.php";
            // require "home.php";

/*        }
        elseif ($logged_in_user['user_type'] == 'user') {

            $_SESSION['user'] = $logged_in_user;
            $_SESSION['success'] = "You are now logged in";
            header('location: home.php');
            isLoggedIn();
        }

*/









    //  $id = 0;

    //return $usersPosts;

 //   global $db, $username, $errors;

    // grap form values
/*    $username = e($_POST['username']);
    $password = e($_POST['password']);

    // make sure form is filled properly
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    // attempt login if no errors on form
    if (count($errors) == 0) {
        $password = md5($password);

        $query = "SELECT * FROM users WHERE username='$username' AND password='$password' LIMIT 1";
        $results = mysqli_query($db, $query);

        if (mysqli_num_rows($results) == 1) { // user found
            // check if user is admin or user
            $logged_in_user = mysqli_fetch_assoc($results);
            if ($logged_in_user['user_type'] == 'admin') {

                $_SESSION['user'] = $logged_in_user;
                $_SESSION['success'] = "You are now logged in";


                /*  header('location: home.php');*/
               // isLoggedIn();
                //require_once "controler/users_controler.php";
               // require "home.php";

/*            }
            elseif ($logged_in_user['user_type'] == 'user') {

                $_SESSION['user'] = $logged_in_user;
                $_SESSION['success'] = "You are now logged in";
                header('location: home.php');
                isLoggedIn();
            }
        }else {
            array_push($errors, "Wrong username/password combination");
        }
    }
}*/


// escape string
/*function e($val){
    global $db;
    return mysqli_real_escape_string($db, trim($val));
}

function display_error() {
    global $errors;

    if (count($errors) > 0){
        echo '<div class="error">';
        foreach ($errors as $error){
            echo $error .'<br>';
        }
        echo '</div>';
    }
}

if (isset($_GET['del_user'])) {
    $id = $_GET['del_user'];
    mysqli_query($db, "DELETE FROM users WHERE id_user=$id");
    $_SESSION['success'] = "user deleted";
    header('location: adminPage.php');
}*/
?>