
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

$password = '121@121';
$hash_method = PASSWORD_ARGON2ID;
$password_encrypted = password_hash($password, constant($hash_method));

echo '<pre>'. print_r($password_encrypted, 1) .'</pre>';*/
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a062562745.js" crossorigin="anonymous"></script>
    <title>MP3 Player</title>
    <link rel=stylesheet href="style.css" media=all>
</head>
<body>


<audio id="myAudio" ontimeupdate="onTimeUpdate()">
    <!-- <source src="audio.ogg" type="audio/ogg"> -->
    <source id="source-audio" src="" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

<div class="player-ctn">
    <div class="infos-ctn">
        <div class="timer">00:00</div>
        <div class="title"></div>
        <div class="duration">00:00</div>
    </div>
    <div id="myProgress">
        <div id="myBar"></div>
    </div>
    <div class="btn-ctn">
        <div class="btn-action first-btn" onclick="previous()">
            <div id="btn-faws-back">
                <i class='fas fa-step-backward'></i>
            </div>
        </div>
        <div class="btn-action" onclick="rewind()">
            <div id="btn-faws-rewind">
                <i class='fas fa-backward'></i>
            </div>
        </div>
        <div class="btn-action" onclick="toggleAudio()">
            <div id="btn-faws-play-pause">
                <i class='fas fa-play' id="icon-play"></i>
                <i class='fas fa-pause' id="icon-pause" style="display: none"></i>
            </div>
        </div>
        <div class="btn-play" onclick="forward()">
            <div id="btn-faws-forward">
                <i class='fas fa-forward'></i>
            </div>
        </div>
        <div class="btn-action" onclick="next()">
            <div id="btn-faws-next">
                <i class='fas fa-step-forward'></i>
            </div>
        </div>
        <div class="btn-mute" id="toggleMute" onclick="toggleMute()">
            <div id="btn-faws-volume">
                <i id="icon-vol-up" class='fas fa-volume-up'></i>
                <i id="icon-vol-mute" class='fas fa-volume-mute' style="display: none"></i>
            </div>
        </div>
    </div>
    <div class="playlist-ctn"></div>
</div>
<script src=app.js></script>

</body>
</html>

<script>  function createTrackItem(index,name,duration){
         var trackItem = document.createElement('div');
         trackItem.setAttribute("class", "playlist-track-ctn");
         trackItem.setAttribute("id", "ptc-"+index);
         trackItem.setAttribute("data-index", index);
         document.querySelector(".playlist-ctn").appendChild(trackItem);

         var playBtnItem = document.createElement('div');
         playBtnItem.setAttribute("class", "playlist-btn-play");
         playBtnItem.setAttribute("id", "pbp-"+index);
         document.querySelector("#ptc-"+index).appendChild(playBtnItem);

         var btnImg = document.createElement('i');
         btnImg.setAttribute("class", "fas fa-play");
         btnImg.setAttribute("height", "40");
         btnImg.setAttribute("width", "40");
         btnImg.setAttribute("id", "p-img-"+index);
         document.querySelector("#pbp-"+index).appendChild(btnImg);

         var trackInfoItem = document.createElement('div');
         trackInfoItem.setAttribute("class", "playlist-info-track");
         trackInfoItem.innerHTML = name
         document.querySelector("#ptc-"+index).appendChild(trackInfoItem);

         var trackDurationItem = document.createElement('div');
         trackDurationItem.setAttribute("class", "playlist-duration");
         trackDurationItem.innerHTML = duration
         document.querySelector("#ptc-"+index).appendChild(trackDurationItem);
     }

     var listAudio = [
         {
             name:"Artist 1 - audio 1",
             file: <?php zip_read('https://ektoplazm.com/files/VA%20-%20Dividing%202%20Worlds%20-%202018%20-%20MP3.zip')?>"p",
             duration:"02:12"
         },
         {
             name:"Artist 2 - audio 2",
             file:"https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_2MG.mp3",
             duration:"00:52"
         },
         {
             name:"Artist 3 - audio 3",
             file:"https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_1MG.mp3",
             duration:"00:27"
         }
     ]

     for (var i = 0; i < listAudio.length; i++) {
         createTrackItem(i,listAudio[i].name,listAudio[i].duration);
     }
     var indexAudio = 0;

     function loadNewTrack(index){
         var player = document.querySelector('#source-audio')
         player.src = listAudio[index].file
         document.querySelector('.title').innerHTML = listAudio[index].name
         this.currentAudio = document.getElementById("myAudio");
         this.currentAudio.load()
         this.toggleAudio()
         this.updateStylePlaylist(this.indexAudio,index)
         this.indexAudio = index;
     }

     var playListItems = document.querySelectorAll(".playlist-track-ctn");

     for (let i = 0; i < playListItems.length; i++){
         playListItems[i].addEventListener("click", getClickedElement.bind(this));
     }

     function getClickedElement(event) {
         for (let i = 0; i < playListItems.length; i++){
             if(playListItems[i] == event.target){
                 var clickedIndex = event.target.getAttribute("data-index")
                 if (clickedIndex == this.indexAudio ) { // alert('Same audio');
                     this.toggleAudio()
                 }else{
                     loadNewTrack(clickedIndex);
                 }
             }
         }
     }

     document.querySelector('#source-audio').src = listAudio[indexAudio].file
     document.querySelector('.title').innerHTML = listAudio[indexAudio].name


     var currentAudio = document.getElementById("myAudio");

     currentAudio.load()

     currentAudio.onloadedmetadata = function() {
         document.getElementsByClassName('duration')[0].innerHTML = this.getMinutes(this.currentAudio.duration)
     }.bind(this);

     var interval1;

     function toggleAudio() {

         if (this.currentAudio.paused) {
             document.querySelector('#icon-play').style.display = 'none';
             document.querySelector('#icon-pause').style.display = 'block';
             document.querySelector('#ptc-'+this.indexAudio).classList.add("active-track");
             this.playToPause(this.indexAudio)
             this.currentAudio.play();
         }else{
             document.querySelector('#icon-play').style.display = 'block';
             document.querySelector('#icon-pause').style.display = 'none';
             this.pauseToPlay(this.indexAudio)
             this.currentAudio.pause();
         }
     }

     function pauseAudio() {
         this.currentAudio.pause();
         clearInterval(interval1);
     }

     var timer = document.getElementsByClassName('timer')[0]

     var barProgress = document.getElementById("myBar");


     var width = 0;

     function onTimeUpdate() {
         var t = this.currentAudio.currentTime
         timer.innerHTML = this.getMinutes(t);
         this.setBarProgress();
         if (this.currentAudio.ended) {
             document.querySelector('#icon-play').style.display = 'block';
             document.querySelector('#icon-pause').style.display = 'none';
             this.pauseToPlay(this.indexAudio)
             if (this.indexAudio < listAudio.length-1) {
                 var index = parseInt(this.indexAudio)+1
                 this.loadNewTrack(index)
             }
         }
     }


     function setBarProgress(){
         var progress = (this.currentAudio.currentTime/this.currentAudio.duration)*100;
         document.getElementById("myBar").style.width = progress + "%";
     }


     function getMinutes(t){
         var min = parseInt(parseInt(t)/60);
         var sec = parseInt(t%60);
         if (sec < 10) {
             sec = "0"+sec
         }
         if (min < 10) {
             min = "0"+min
         }
         return min+":"+sec
     }

     var progressbar = document.querySelector('#myProgress')
     progressbar.addEventListener("click", seek.bind(this));


     function seek(event) {
         var percent = event.offsetX / progressbar.offsetWidth;
         this.currentAudio.currentTime = percent * this.currentAudio.duration;
         barProgress.style.width = percent*100 + "%";
     }

     function forward(){
         this.currentAudio.currentTime = this.currentAudio.currentTime + 5
         this.setBarProgress();
     }

     function rewind(){
         this.currentAudio.currentTime = this.currentAudio.currentTime - 5
         this.setBarProgress();
     }


     function next(){
         if (this.indexAudio <listAudio.length-1) {
             var oldIndex = this.indexAudio
             this.indexAudio++;
             updateStylePlaylist(oldIndex,this.indexAudio)
             this.loadNewTrack(this.indexAudio);
         }
     }

     function previous(){
         if (this.indexAudio>0) {
             var oldIndex = this.indexAudio
             this.indexAudio--;
             updateStylePlaylist(oldIndex,this.indexAudio)
             this.loadNewTrack(this.indexAudio);
         }
     }

     function updateStylePlaylist(oldIndex,newIndex){
         document.querySelector('#ptc-'+oldIndex).classList.remove("active-track");
         this.pauseToPlay(oldIndex);
         document.querySelector('#ptc-'+newIndex).classList.add("active-track");
         this.playToPause(newIndex)
     }

     function playToPause(index){
         var ele = document.querySelector('#p-img-'+index)
         ele.classList.remove("fa-play");
         ele.classList.add("fa-pause");
     }

     function pauseToPlay(index){
         var ele = document.querySelector('#p-img-'+index)
         ele.classList.remove("fa-pause");
         ele.classList.add("fa-play");
     }


     function toggleMute(){
         var btnMute = document.querySelector('#toggleMute');
         var volUp = document.querySelector('#icon-vol-up');
         var volMute = document.querySelector('#icon-vol-mute');
         if (this.currentAudio.muted == false) {
             this.currentAudio.muted = true
             volUp.style.display = "none"
             volMute.style.display = "block"
         }else{
             this.currentAudio.muted = false
             volMute.style.display = "none"
             volUp.style.display = "block"
         }
     }</script>    <li class="plyr__cite plyr__cite--audio" hidden>
                        <small>
                            <svg class="icon" title="HTML5">
                                <title>HTML5</title>
                                <path
                                    d="M14.738.326C14.548.118 14.28 0 14 0H2c-.28 0-.55.118-.738.326S.98.81 1.004 1.09l1 11c.03.317.208.603.48.767l5 3c.16.095.338.143.516.143s.356-.048.515-.143l5-3c.273-.164.452-.45.48-.767l1-11c.026-.28-.067-.557-.257-.764zM12 4H6v2h6v5.72l-4 1.334-4-1.333V9h2v1.28l2 .666 2-.667V8H4V2h8v2z"
                                ></path>
                            </svg>
                            <a href="http://www.kishibashi.com/" target="_blank"
                                >Kishi Bashi &ndash; &ldquo;It All Began With A Burst&rdquo;</a
                            >
                            &copy; Kishi Bashi
                        </small>
                    </li>
                    <li class="plyr__cite plyr__cite--youtube" hidden>
                        <small>
                            <a href="https://www.youtube.com/watch?v=bTqVqk7FSmY" target="_blank"
                                >View From A Blue Moon</a
                            >
                            on&nbsp;
                            <span class="color--youtube">
                                <svg class="icon" role="presentation">
                                    <title>YouTube</title>
                                    <path
                                        d="M15.8,4.8c-0.2-1.3-0.8-2.2-2.2-2.4C11.4,2,8,2,8,2S4.6,2,2.4,2.4C1,2.6,0.3,3.5,0.2,4.8C0,6.1,0,8,0,8
                                   s0,1.9,0.2,3.2c0.2,1.3,0.8,2.2,2.2,2.4C4.6,14,8,14,8,14s3.4,0,5.6-0.4c1.4-0.3,2-1.1,2.2-2.4C16,9.9,16,8,16,8S16,6.1,15.8,4.8z
                                    M6,11V5l5,3L6,11z"
                                    ></path></svg
                                >YouTube
                            </span>
                        </small>
                    </li>
<?php

echo 'Argon2i hash: ' . password_hash('rasmuslerdorf', PASSWORD_ARGON2I);

echo '<br>';
echo '<br>';
echo '<br>';
$timeZone = 'Europe/Paris';  // +2 hours
date_default_timezone_set($timeZone);
echo '<br>';
echo '<br>';
$dateSrc = '2007-04-19 12:50 GMT';
$dateTime = new DateTime($dateSrc);

echo 'date(): '.date('H:i:s', strtotime($dateSrc));
// correct! date(): 14:50:00
echo '<br>';
echo '<br>';
echo 'DateTime::format(): '.$dateTime->format('H:i:s');
// INCORRECT! DateTime::format(): 12:50:00
echo '<br>';
echo '<br>';
$timeZone = 'Europe/Warsaw';  // +2 hours
$dateSrc = '2007-04-19 12:50 GMT';

$dateTime = new DateTime($dateSrc);
$dateTime->setTimeZone(new DateTimeZone($timeZone));
echo 'DateTime::format(): '.$dateTime->format('H:i:s');
// CORRECT! DateTime::format(): 14:50:00
echo '<br>';
echo '<br>';

$today = getdate();
$time=$today['hours'].':'.$today['minutes'].':'.$today['seconds'];
echo $time;

$query = 'SELECT count(1) as cnt FROM user_posts WHERE id =1';


$db = mysqli_connect('localhost', 'root', '', 'multi_login');
$result = mysqli_query($db,$query);
$row = mysqli_fetch_assoc($result);
$count = $row['cnt'];

$pages = ceil($count/10.0); //used to display page links. It's the total number of pages.
$page = 3; //get it from somewhere else, it's the page number.



$query = 'SELECT count(*) as cnt FROM user_posts ';
$result = mysqli_query( $db,$query);
while($row = mysqli_fetch_assoc($result))
{
echo $time;
}
?>

<br><br><br><br>

<?php
$result = mysqli_query($db,"SELECT * FROM user_posts" ); //Note if you have a very large table you probably want to get the count instead of selecting all of the data...
$nrResults = mysqli_num_rows( $result );
/*if( $_GET['page'] ) {
    $page = $_GET['page'];
} else {
    $page = 1;
}*/
$per_page = 2;
$offset = ($page - 1) * $per_page; //So that page 1 starts at 0, page 2 starts at 2 etc.
$result = mysqli_query($db, "SELECT * FROM user_posts LIMIT $offset,$per_page");
while( $line = mysqli_fetch_array( $result ) )
{
echo 'yes';
    //Do whatever you want with each row in here
}

?>



<?php
$connect = mysqli_connect("localhost", "root", "", "testing");
if(isset($_POST["insert"]))
{
    $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));
    $query = "INSERT INTO tbl_images(name) VALUES ('$file')";
    if(mysqli_query($connect, $query))
    {
        echo '<script>alert("Image Inserted into Database")</script>';
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Webslesson Tutorial | Insert and Display Images From Mysql Database in PHP</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<br /><br />
<div class="container" style="width:500px;">
    <h3 align="center">Insert and Display Images From Mysql Database in PHP</h3>
    <br />
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="image" id="image" />
        <br />
        <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-info" />
    </form>
    <br />
    <br />
    <table class="table table-bordered">
        <tr>
            <th>Image</th>
        </tr>
        <?php
        $query = "SELECT * FROM tbl_images ORDER BY id DESC";
        $result = mysqli_query($connect, $query);
        while($row = mysqli_fetch_array($result))
        {
            echo '  
                          <tr>  
                               <td>  
                                    <img src="data:image/jpeg;base64,'.base64_encode($row['name'] ).'" height="200" width="200" class="img-thumnail" />  
                               </td>  
                          </tr>  
                     ';
        }
        ?>
    </table>
</div>
</body>
</html>
<script>
    $(document).ready(function(){
        $('#insert').click(function(){
            var image_name = $('#image').val();
            if(image_name == '')
            {
                alert("Please Select Image");
                return false;
            }
            else
            {
                var extension = $('#image').val().split('.').pop().toLowerCase();
                if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
                {
                    alert('Invalid Image File');
                    $('#image').val('');
                    return false;
                }
            }
        });
    });
</script>

