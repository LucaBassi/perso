<?php

include('functions.php');

if (!isLoggedIn()) {
    $_SESSION['msg'] = " to see this page ->You must log in first ";
    header('location: login.php');

}

ob_start();
?>

    <div class="inner">

        <section >

            <div class="inner">
                <section class="inner">

                    <table>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nom</th>
                            <th>Addresse mail</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <?php $userResults = mysqli_query($db, "SELECT * FROM users"); ?>
                        <?php while ($row = mysqli_fetch_array($userResults)) {
                            $nbCol=0;
                            $nbCol++;
                        ?>
                            <tr>
                                <td><?php echo $nbCol++; ?></td>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $row['email']; ?></td>
                                <td><?php echo $row['user_type']; ?></td>
                                <td></td>
                            </tr>
                        <?php
                       } ?>
                    </table>
                </section>
            </div>
        </section>
    </div>


<?php
$contenu = ob_get_clean();
require "gabarit.php";

