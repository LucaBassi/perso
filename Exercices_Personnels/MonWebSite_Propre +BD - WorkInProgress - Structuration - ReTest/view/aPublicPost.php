<?php ob_start(); ?>


    <section id="contact">
        <h3>Billets de Blog </h3>
        <div class="inner">

            <section>
                <div class="image" style="margin-left: 33%">
                    <?php if ($post['image'] != "") { ?>
                        <img src="data:image/jpeg;base64,<?= base64_encode($post['image']) ?>" height="300" width="300"
                             class="img-thumnail"/>
                    <?php } ?>
                    </div>   <div>

                        <textarea rows="12"><?= $post['post'] ?></textarea>


                    </div>


            </section>
            <section>
                <div class="inner">
                    <table>

                        <thead>
                        <tr>
                            <td>
                                <h3>Commentaires</h3>
                            </td>
                        </tr>
                        </thead>
                        <tr>
                    </table>

                    <?php $index = 0;
                    foreach ($comments as $comment):
                        $index++ ?>
                        <table>

                            <thead>
                            <tr>
                                <td>
                                    <b> posté par : </b>  <h6><?= $comment['autor'] ?></h6>
                                </td>
                                <td>
                                    <b> Date : </b>  <h6><?= $comment['date'] ?></h6>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">
                                    <p><?= $comment['comment'] ?></p>
                                </td>
                            </tr>
                            </tbody>
                            <tr>
                        </table>
                        <h6>

                        </h6>
                        <?php
                        if ($index == 2) {
                            break;
                        }
                        ?>
                    <?php endforeach; ?>


            </section>
        </div>

    </section>

    <form method="post" action="index?action=comment">

        <label style="color: #6d3353">Pseudo</label>
        <h4 type="text" name="name" value=""></h4>
        <textarea style="border-block: #0e0e0e" type="text" name="comment"></textarea>
        <label style="color: #6d3353">Post</label>
        <button class="button small" type="submit">Comment</button>
        <label>
            <input type="hidden" name="id_comment" value=<?= $id ?>>
        </label>
        <label>
            <input type="hidden" name="id" value=<?= $_POST['id'] ?>>
        </label>

    </form>


<?php
$contenu = ob_get_clean();
require_once "gabarit.php";
?>