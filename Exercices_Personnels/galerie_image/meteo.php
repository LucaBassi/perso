<script>
    onload.window = getLocation();
</script>
<?php
if (isset($_GET)) {
    extract($_GET);
}

$url = 'https://www.prevision-meteo.ch/services/json/ste-croix';

if (isset($_GET['actionMeteo']))
    switch ($_GET['actionMeteo']) {
        case '1' :
            $url = 'https://www.prevision-meteo.ch/services/json/ste-croix';
            break;
        case '2' :
            $url = 'https://www.prevision-meteo.ch/services/json/nyon';
            break;

        case '3' :
            $url = 'https://www.prevision-meteo.ch/services/json/concise';
            break;

        case '4' :
            $url = 'https://www.prevision-meteo.ch/services/json/geneve';
            break;

        case '5' :
            $url = 'https://www.prevision-meteo.ch/services/json/appenzell';
            break;

        case '6' :
            $url = 'https://www.prevision-meteo.ch/services/json/sierre';
            break;

        case '7' :
            $url = 'https://www.prevision-meteo.ch/services/json/frauenfeld';
            break;

        case '8' :
            $url = 'https://www.prevision-meteo.ch/services/json/zermatt';
            break;

        case '9' :
            $url = 'https://www.prevision-meteo.ch/services/json/bern';
            break;

        case 'okLoc' :
            $loc = $_POST['searchLoc'];
            $urlToGo = 'https://www.prevision-meteo.ch/services/json/' . $loc;
            $url = $urlToGo;
            break;

        case 'myLoc' :
            $mylocGPS = "lat=" . $_POST['myLat'] . "lng=" . $_POST['myLong'];
            $urlToGo = 'https://www.prevision-meteo.ch/services/json/' . $mylocGPS;
            $url = $urlToGo;
            break;

        default :
            meteo();
            break;
    }

$jsonfile = file_get_contents($url);
$json = json_decode($jsonfile);
ob_start();
$date = getdate();
?>

<div style="text-align: center">
    <h4>
        <img style="width: 3%" src="Image/cone.png"/>
        Cette page doit avoir l'extention "openssl" doit être activée dans le fichier php.ini
        <img style="width: 3%" src="Image/cone.png"/>
    </h4>
    <div style=" margin-left: 25%;margin-right: 25%">
        <table style="text-align: center">
            <thead>
            <tr>
                <h3>Date et Heure</h3>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="  border: 3px solid black;
  border-collapse: collapse;
border-radius: 6px ">
                    <h4>
                        <?php
                        $wday[1] = 'Lundi';
                        $wday[2] = 'Mardi';
                        $wday[3] = 'Mercredi';
                        $wday[4] = 'Jeudi';
                        $wday[5] = 'Vendredi';
                        $wday[6] = 'Samedi';
                        $wday[0] = 'Dimanche';
                        $today = $wday[$date['wday']];
                        echo $today . " " . $date['mday'] . " " . $date['month'] . " " . $date['year'];
                        ?>
                    </h4>
                </td>
            </tr>
            <tr>
                <td style="  border: 3px solid black;border-collapse: collapse;border-radius: 6px ">
                    <h4 id="date_heure"></h4>
                </td>
            </tr>

            <tr>
                <td style="  border: 3px solid black;border-collapse: collapse;border-radius: 6px ">
                    <div>
                        <h4 style="text-align: center">Cherher une localité</h4>
                        <form method="post" action="meteo.php?actionMeteo=okLoc">
                            <input type="text" name="searchLoc" class="input-search" placeholder="Search">
                            <button type="submit" class="cffButton2" value=>Search
                        </form>
                    </div>
                </td>
            </tr>

            <tr>
                <td style="  border: 3px solid black;border-collapse: collapse;border-radius: 6px ">
                    <div>
                        <h4>Geolocalisation</h4>
                        <button onclick="getLocation()">Obtenez vos info GPS</button>
                        <form method="post" action="meteo.php?actionMeteo=myLoc">
                            <input name="myLat"
                                   value="<?php echo $lat = (isset($_GET['lat'])) ? number_format($_GET['lat'], 4) : ''; ?>">
                            <input name="myLong"
                                   value="<?php echo $long = (isset($_GET['long'])) ? number_format($_GET['long'], 4) : ''; ?>">
                            <button type="submit" class="cffButton2">go</button>
                        </form>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
    <script>
        function date_heure(id) {
            date = new Date;

            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            resultat = ' Il est ' + h + ':' + m + ':' + s;
            document.getElementById(id).innerHTML = resultat;
            setTimeout('date_heure("' + id + '");', '1000');
            return true;
        }

    </script>


    <script type="text/javascript">window.onload = date_heure('date_heure');</script>

    <?php if (isset($json->current_condition->icon_big)){ ?>
    <img src="<?php echo $json->current_condition->icon_big; ?>">
    <h2>    <?php echo $json->current_condition->tmp, "°C"; ?></h2>
    <?php if ($json->city_info->name == "NA") { ?>
        <h3> Temps et temp. à votre position actuelle</h3>

    <?php } else { ?>
        <h2> Temps et temp. à : <?php echo $json->city_info->name; ?></h2>
    <?php } ?>


    <div style="margin-right: 8%">
        <table style="margin-left: 5%">
            <thead>
            <tr>
                <FORM method="post" action="meteo.php" NAME="ChoixVille"
                ">
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=1">Ste-Croix</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=2">Nyon</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=3">Concise</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=4">Geneve</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=5">Appenzell</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=6">Sierre</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=7">Frauenfeld</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=8">Zermatt</a></h4></th>
                <th><h4><a methods="get" class="ButtonMenu" href="meteo.php?actionMeteo=9">Bern</a></h4></th>
                </FORM>
            </tr>

            </thead>

        </table>

    </div>
</div>


<div class="bodyBackground1New">

    <table>
        <div style="text-align: center">
            <table>
                <thead>
                <tr>
                    <th style="width:15%"><h2>Journee</h2></th>
                    <th style="width:15%"><h2>General</h2></th>
                    <th><h2>Matin</h2></th>
                    <th style="width:15%"><h2>Apres-midi</h2></th>
                    <th><h2>Soirée</h2></th>
                    <th><h2>Min/Max °C</h2></th>
                    <th style="width:10%"><h2>Pluie mm</h2></th>

                </tr>
                </thead>
                <tbody style="margin-left: 20%">
                <tr class="table-wrapper">
                    <td><h3><?php echo $json->fcst_day_0->day_long; ?></h3></td>
                    <td><h3><?php echo $json->fcst_day_0->condition; ?></h3></td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'9H00'}->ICON; ?>"></h3></td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'14H00'}->ICON; ?>"</h3></td>
                    <td><img src="<?php echo $json->fcst_day_0->hourly_data->{'20H00'}->ICON; ?>"</h3></td>
                    <td style="width: 15%;text-align: center">
                        <h3><?php echo " de   ", $json->fcst_day_0->tmin, "  à  ", $json->fcst_day_0->tmax, "°C"; ?></h3>
                    </td>
                    <td style="text-align: center">
                        <h3><?php echo $json->fcst_day_0->hourly_data->{'0H00'}->APCPsfc, " mm."; ?></h3></td>
                </tr>

                <tr>
                    <td><h3><?php echo $json->fcst_day_1->day_long; ?></td>
                    <td><h3><?php echo $json->fcst_day_1->condition; ?></td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'9H00'}->ICON; ?>"></td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'14H00'}->ICON; ?>"</td>
                    <td><img src="<?php echo $json->fcst_day_1->hourly_data->{'20H00'}->ICON; ?>"</td>
                    <td style="width: 15%;text-align: center">
                        <h3><?php echo " de   ", $json->fcst_day_1->tmin, "  à  ", $json->fcst_day_1->tmax, "°C"; ?>
                    </td>
                    <td style="text-align: center">
                        <h3><?php echo $json->fcst_day_1->hourly_data->{'0H00'}->APCPsfc, " mm."; ?></td>
                </tr>

                <tr>
                    <td><h3><?php echo $json->fcst_day_2->day_long; ?></td>
                    <td><h3><?php echo $json->fcst_day_2->condition; ?></td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'9H00'}->ICON; ?>"></td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'14H00'}->ICON; ?>"</td>
                    <td><img src="<?php echo $json->fcst_day_2->hourly_data->{'20H00'}->ICON; ?>"</td>
                    <td style="width: 15%;text-align: center">
                        <h3><?php echo " de ", $json->fcst_day_2->tmin, "  à  ", $json->fcst_day_2->tmax, "°C"; ?></td>
                    <td style="text-align: center">
                        <h3><?php echo $json->fcst_day_2->hourly_data->{'0H00'}->APCPsfc, " mm."; ?></td>
                </tr>

                <tr>
                    <td><h3><?php echo $json->fcst_day_3->day_long; ?></td>
                    <td><h3><?php echo $json->fcst_day_3->condition; ?></td>
                    <td><img src="<?php echo $json->fcst_day_3->hourly_data->{'9H00'}->ICON; ?>"></td>
                    <td><img src="<?php echo $json->fcst_day_3->hourly_data->{'14H00'}->ICON; ?>"</td>
                    <td><img src="<?php echo $json->fcst_day_3->hourly_data->{'20H00'}->ICON; ?>"</td>
                    <td style="width: 15%;text-align: center">
                        <h3><?php echo " de ", $json->fcst_day_3->tmin, "  à  ", $json->fcst_day_3->tmax, "°C"; ?></td>
                    <td style="text-align: center">
                        <h3><?php echo $json->fcst_day_3->hourly_data->{'0H00'}->APCPsfc, " mm."; ?></td>
                </tr>

                <tr>
                    <td><h3><?php echo $json->fcst_day_4->day_long; ?></td>
                    <td><h3><?php echo $json->fcst_day_4->condition; ?></td>
                    <td><img src="<?php echo $json->fcst_day_4->hourly_data->{'9H00'}->ICON; ?>"></td>
                    <td><img src="<?php echo $json->fcst_day_4->hourly_data->{'14H00'}->ICON; ?>"</td>
                    <td><img src="<?php echo $json->fcst_day_4->hourly_data->{'20H00'}->ICON; ?>"</td>
                    <td style="width: 15%;text-align: center">
                        <h3><?php echo " de ", $json->fcst_day_4->tmin, "  à  ", $json->fcst_day_4->tmax, "°C"; ?></td>
                    <td style="text-align: center">
                        <h3><?php echo $json->fcst_day_4->hourly_data->{'0H00'}->APCPsfc, " mm."; ?></td>
                </tr>
                </tbody>
                <tfoot>
                <a href="https://www.prevision-meteo.ch">
                    <p>
                        -> Les donnée météos présentes ici proviennent de : www.prevision-meteo.ch <-
                    </p>
                </a>
                </tfoot>
            </table>
        </div>
    </table>
    <div>


        <?php }
        else {
            echo '<h2>Localité non valide :(</h2>'; ?>
            <div style="text-align: center">
                <form method="post" action="meteo.php?actionMeteo=okLoc">
                    <input type="text" name="searchLoc" class="input-search" placeholder="Search">
                    <button type="submit" class="btn-search" value=>Search
                </form>
            </div>
            <a methods="get" class="ButtonMenu" href="index.php?action=meteo">retourner à la pasge Meteo</a>

        <?php } ?>


        <script>
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(redirectToPosition);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }

            function redirectToPosition(position) {
                window.location = 'meteo.php?lat=' + position.coords.latitude + '&long=' + position.coords.longitude;
            }

            LANGUAGE = "JavaScript" >
                function HeureCheckEJS() {
                    krucial = new Date;
                    heure = krucial.getHours();
                    min = krucial.getMinutes();
                    sec = krucial.getSeconds();

                    if (sec < 10)
                        sec0 = "0";
                    else
                        sec0 = "";
                    if (min < 10)
                        min0 = "0";
                    else
                        min0 = "";
                    if (heure < 10)
                        heure0 = "0";
                    else
                        heure0 = "";
                    DinaHeure = heure0 + heure + ":" + min0 + min + ":" + sec0 + sec;
                    which = DinaHeure
                    if (document.getElementById) {
                        document.getElementById("ejs_heure").innerHTML = which;
                    }
                    setTimeout("HeureCheckEJS()", 1000)
                }
            window.onload = HeureCheckEJS;
        </SCRIPT>
<?php
$contenu = ob_get_clean();
require "gabarit.php";

