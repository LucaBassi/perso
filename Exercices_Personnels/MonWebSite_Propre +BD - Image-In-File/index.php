<?php


require 'controler.php';
//require 'Vue/css/style.css';

if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        case 'details' :
            details();
            break;

        case 'nouveau' :
            nouveau();
            break;

        case 'meteo' :
            meteo();
            break;

        case 'transports' :
            transports();
            break;

        case 'listeDePresence' :
            listeDePresence();
            break;

        case 'wantedLogin' :
            wantedLogin();
            break;

        case 'goToUploadImage' :
            goToUploadImage();
            break;

        case 'listeImage' :
            listeImage();
            break;

        case 'posts' :
            posts();
            break;

        case 'userPage' :
            userPage();
            break;

      case 'adminPage' :
            adminPage();
            break;

        default :
            home();
            break;

    }
}
else
{
    home();
}
