<?php

session_start();

require 'controler.php';
//require 'Vue/css/style.css';

if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        case 'details' :
            details();
            break;

        case 'nouveau' :
            nouveau();
            break;

        case 'meteo' :
            meteo();
            break;

        case 'transports' :
            transports();
            break;

        case 'listeDePresence' :
            listeDePresence();
            break;

        case 'wantedLogin' :
            wantedLogin();
            break;

        default :
            home();
            break;

        case 'home' :
            home();
            break;
    }
}
else
{
    home();
}
