<?php
require("nouveau.php");

// This function simply returns some hardcoded data

function getData()
{
    return json_decode('{ "employees" : [{ "firstname":"John" , "lastname":"Doe", "age":24 , "photo": "Image/photos_personnes/personne0.jpg","typeAbonnement": [{"type1": "Demi Tarif"},{"type2": "Plein Tarif"}]},{ "firstname":"Anna" , "lastname":"Smith", "photo": "Image/photos_personnes/personne2.jpg" }, { "firstname":"Peter" , "lastname":"Jones", "photo": "Image/photos_personnes/personne3.jpg" } ]}', true);
}

// ============== Load or create data ================

$dataDirectory = "data";
$dataFileName = "mydata.json";

if (file_exists("$dataDirectory/$dataFileName")) // the file already exists -> load it
{
    $data = json_decode(file_get_contents("$dataDirectory/$dataFileName"), true);
} else {
    if (!file_exists($dataDirectory)) // Check if data directory exists
    {
        mkdir($dataDirectory); // if not create it
    }
    $data = getData(); // Initialize data with fixed values
}
echo $data['personnes'][0]['lastname'];
// ============== Process commands from GET parameters ================
extract($_GET); // possible variables created:  $init, $create, $update, $delete, $index, $firstname, $lastname, $hobby
//                              $mail,$noIdentite,$rue,$numero,$country,$city,$npa, photoPersonne;

extract($_POST);
// --- 1. init
if (isset($init)) // reinitialise data
{
    $data = getData(); // Initialize data with fixed values
    echo "Données réinitialisées";
}

// --- 2. delete
if (isset($delete)) // delete the person of the array who is at index "$index"
{
    echo "Suppression de " . $data[$index]['Firstname'] . "<br>";

    for ($i = $index; $i < count($data) - 1; $i++) // shift all elements beyond the one we must delete
    {
        $data[$i] = $data[$i + 1];
    }
    unset($data[$i]); // destroy the last one
}


if (isset($firstname)) {

    $json_decode_str = $data;
    $my_index = 1;
    foreach ($json_decode_str['0']['personnes'] as $personne) {


        if ($my_index == 1) {
            //  echo $personne['firstname'];
        }

        $my_index++;


    }
    //  echo $my_index;

    /*for ($i = $index; $i < count($data[0]) - 1; $i++) // shift all elements beyond the one we must delete
    {
        $data[$index]++;
        $data[$i] = $data[$index];
        $index = $i;
        $index++;
        //  $j=$index;
    }*/

    //echo  $friend = [$my_index]['personnes'];
    // echo "Modification de ".$friend['firstname'];

    $friend['id'] = $my_index;

    if (isset($firstname)) // a first name was given in the querystring
    {
        $friend['firstname'] = $firstname;
    }
    if (isset($lastname)) // a last name was given in the querystring
    {
        $friend['lastname'] = $lastname;
    }
    if (isset($age)) {
        $friend['age'] = $age;
    }
    if (isset($mail)) {
        $friend['mail'] = $mail;
    }
    if (isset($noIdentite)) {
        $friend['noIdentite'] = $noIdentite;
    }
    if (isset($rue)) {
        $friend['address']['0']['rue'] = $rue;
    }
    if (isset($numero)) {
        $friend['address']['0']['numero'] = $numero;
    }
    if (isset($country)) // a hobby was given in the querystring
    {
        $friend['address']['1']['country'] = $country;
    }
    if (isset($city)) // a hobby was given in the querystring
    {
        $friend['address']['1']['city'] = $city;
    }
    if (isset($npa)) // a hobby was given in the querystring
    {
        $friend['address']['1']['npa'] = $npa;
    }

    if (isset($conditions)) // a hobby was given in the querystring
    {
        $friend['conditions'] = $conditions;
    }

    if (isset($typeAbonnement)) // a hobby was given in the querystring
    {
        $friend['typeAbonnement'] = $typeAbonnement;
    }
    if (isset($message)) // a hobby was given in the querystring
    {
        $friend['messagePerso'] = $message;
    }


    //if(isset($_POST['submitform']))
    $dir = "upload/";
    $image = $_FILES['uploadfile']['name'];
    $temp_name = $_FILES['uploadfile']['tmp_name'];

    if ($image != "") {
        if (file_exists($dir . $image)) {
            $image = time() . '_' . $image;
        }

        //rename image like : "index-firstname-lastname-imagename.extension" and add is relative id (from my_index out of foreach)
        //$extensionFile=$image['extension'];
        $image = $my_index . '-' . $friend['firstname'] . '-' . $friend['lastname'] . '-' . $image;
        $fdir = $dir . $image;
        move_uploaded_file($temp_name, $fdir);

        $friend['photo'] = $fdir;


        /*}

            echo "File Uploaded Suucessfully ";*/

    }


}


$data[0]['personnes'][] = $friend; // save

//  for ($j=0; $i < count($data)-1; $i++) // shift all elements beyond the one we must delete
//{
//    $index=$j;
//}
//    $friend[$i]['personnes']="ssssss";


// ============== Save data ================

file_put_contents("$dataDirectory/$dataFileName", json_encode($data));

// ============== Display data ================


?>
<!--
<!DOCTYPE html>
<html>
<head>
	<title>File Upload to Database</title>
</head>
<body>

			<div>
						<h1>File Upload with PHP and MySQLI</h1>

						<form action="" method="post" enctype="multipart/form-data">

							Upload Images/File : <input type="file" name="uploadfile">

						   </br>

						    <button type="submit" name="submitform">Upload</button>
						</form>
			</div>-->

<!--
			<div>
					<h2>Show All Upload Images</h2>

					<table border="1" cellpadding="2" cellspacing="0">
							<tr>
									<th>Sr.NO</th>
									<th>Name</th>
							</tr>
					<?php
/*							$i=1;
							$sql="select * from `images`";
                 //   $con=mysqli_connect("localhost","root", "","test");
							//$qry=mysqli_query($con,$sql) or die(mysqli_error($con));

							//while($row=mysqli_fetch_array($qry))
							{

					*/ ?>
							<tr>
									<td><?php /* echo $i;*/ ?></td>
									<td><img src="upload/<?php /*//echo $row['file'];*/ ?>" width="100" height="100"></td>
							</tr>
				 <?php
/*				 			$i++;
				 		}
				 */ ?>
					</table>
			</div>
-->
