<?php


require 'controler/users_controler.php';
//require 'Vue/css/style.css';

if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        case 'details' :
            details();
            break;

        case 'nouveau' :
            nouveau();
            break;

        case 'meteo' :
            meteo();
            break;

        case 'transports' :
            transports();
            break;

        case 'listeDePresence' :
            listeDePresence();
            break;

        case 'wantedLogin' :
            wantedLogin();
            break;

        case 'goLogin' :
            goLogin();
            break;

        case 'goToUploadImage' :
            goToUploadImage();
            break;

        case 'listeImage' :
            listeImage();
            break;

        case 'posts' :
            require_once 'controler/posts_controler.php';
            posts();
            break;

        case 'mesPosts' :
            require_once 'controler/posts_controler.php';
            mesPosts();
            break;

        case 'showAPost' :
            require_once 'controler/posts_controler.php';
            showAPost();
            break;

        case 'updateAPost' :
            require_once 'controler/posts_controler.php';
            updateAPost();
            break;

      case 'adminPage' :
            adminPage();
            break;

        case 'userPage' :
            userPage();
            break;

       case 'checkLogin' :
           checkLogin();
            break;

        case 'login' :
            login($_POST);
            break;

        case 'register' :
            register($_POST);
            break;

        case 'logout' :
            logout();
            break;


        default :
            home();
            break;

    }
}
else
{
    home();
}
